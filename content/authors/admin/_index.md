---
authors:
- admin
bio:
education:
  courses:
  - course: PhD in Biostatistics
    institution: University of Leicester, UK
    year: 1999
  - course: MSc in Medical Statistics
    institution: University of Leicester, UK
    year: 1992

email: "pclt@kreftregisteret.no"
interests:
- Survival Analysis
- Epidemiology
- Cancer
- Statistical Software
name: Paul C Lambert
organizations:
- name: Cancer Registry of Norway / Karolinska Institutet
  url: "le.ac.uk"
role: Biostatistician
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/pclambert123
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=5AjjPL4AAAAJ&hl=en
- icon: github
#  icon_pack: fab
#  link: https://github.com/gcushen
superuser: true
user_groups:
- Researchers
- Visitors
---

I am a biostatistician working at the [Cancer Registry of Norway](https://www.kreftregisteret.no/en/). I also work part-time as a Guest Professor in the [Biostatistics Group](http://ki.se/en/meb/biostatisticians) in the Department of Medical Epidemiology and Biostatistics, Karolinska Institutet, Stockholm, Sweden. Previously, I worked at the [Biostatistics Research Group](http://www2.le.ac.uk/departments/health-sciences/research/biostats) at the University of Leicester, UK.


I have a variety of interests, mainly in the area of survival analysis in epidemiology. Much of my work focuses on the analysis from data from national cancer registries. I particularly enjoy writing [software]({{< ref "#software" >}}) to enable transferal of new methods into practice.

I enjoy [teaching]({{< ref "#teaching" >}}) and have developed some [interactive graphs](interactivegraphs/) to help with communication. 


