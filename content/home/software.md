+++
# A Recent and Upcoming Talks section created with the Pages widget.
# This section displays recent talks from `content/talk/`.

date = "2021-06-11"
draft = false

title = "Software"
subtitle = ""
widget = "custom"
math = true
# Order that this section will appear in.
weight = 60
+++

I have developed a number of Stata commands. I have added some examples of using this code and intend to add to these over time.

- [**stpm2**](/software/stpm2/) - flexible parametric survival models
- [**stpm3**](/software/stpm3/) - flexible parametric survival models
- [**standsurv**](/software/standsurv/) - standardized survival curves and more after fitting various types of survival models.
- [**mrsprep**](/software/mrsprep/) - prepare data to directly fit marginal relative survival models.
- [**rcsgen**](/software/rcsgen.md)  - generate restricted cubic splines
- [**stpm2_standsurv**](/software/stpm2_standsurv) - standardized survival curves after fitting an `stpm2` model
- [**stpp**](/software/stpp) - Non-parametric marginal relative (net) survival using Pohar Perme estimator
- [**mlad**](/software/mlad) - Maximum likelIhood estimation with automatic differentiation using Python
- **stpm2cif** - cause-specific cumulative incidence function after fitting a stpm2 competing risks model
- [**stcrprep**](/software/stcrprep) - data-preparation command to fit a range of competing risks models.
- **partpred** - partial predictions
- **strcs** - flexible parametric model on log hazard scale

I have worked with [Paul Dickman](http://pauldickman.com/), who has written some excellent [Stata tutorials](http://pauldickman.com/software/stata/), many of which use my commands.