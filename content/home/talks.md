+++
# A Recent and Upcoming Talks section created with the Pages widget.
# This section displays recent talks from `content/talk/`.

widget = "pages"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "Recent & Upcoming Talks"
subtitle = ""

[content]
  # Page type to display. E.g. post, talk, or publication.
  page_type = "talk"
  
  # Choose how much pages you would like to display (0 = all pages)
  count = 5
  
  # Choose how many pages you would like to offset by
  offset = 0

  # Page order. Descending (desc) or ascending (asc) date.
  order = "desc"

  # Filter posts by a taxonomy term.
  [content.filters]
    tag = ""
    category = ""
    publication_type = ""
    exclude_featured = false
    exclude_past = false
    exclude_future = false
    
[design]
  # Toggle between the various page layout types.
  #   1 = List
  #   2 = Compact
  #   3 = Card
  #   4 = Citation (publication only)
  view = 2
  
[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  # gradient_start = "DeepSkyBlue"
  # gradient_end = "SkyBlue"
  
  # Background image.
  # image = "background.jpg"  # Name of image in `static/img/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  # text_color_light = true  
  
[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = ""
+++

A list of selected older talks can be found [here](alltalks/)

> **Recent (and not so recent) Talks:**  

>10 September 2024 [Improving the speed and accuracy when fitting flexible parametric survival models on the log
hazard scale](pdf/Stata2024_Oslo_Paul_Lambert.pdf) 2024 Northern European Stata Conference, Oslo, Norway

>30 August 2024 [A practical approach to fitting cancer survival models when data can’t move across borders](pdf/Paul_Lambert_ANCR2024.pdf) Association of Nordic Cancer Registries (ANCR) Symposium 2024, Bodø, Norway

>7 June 2024 [Recent developments in the fitting and assessment of flexible parametric survival models](pdf/Stata2024_Germany_Paul_Lambert.pdf) 2024 German Stata Conference

>12 October 2022 [Improving fitting and predictions for flexible parametric survival models](Improving fitting and predictions for flexible parametric survival models) Northern European Stata Conference

>9 September 2022 [Improving fitting and predictions for flexible parametric survival models](http://repec.org/lsug2022/uk2022_lambert.html#/title-slide)
2022 UK Stata Conference

>6 August 2021. [Making Stata estimation commands faster through automatic differentiation and integration with Python](pdf/Stata2021_Paul_Lambert.pdf). 2021 Stata Conference

>18 February 2021. [Regression standardization with time-to-event data to estimate marginal measures of association and causal effects using the `standsurv` command.](/pdf/Stata_biostatsepi_2021_Paul_Lambert.pdf)

>22 September 2020. [Standardised and reference adjusted all-cause and crude probabilities in the relative survival framework](pdf/Biometric_Society_Sept2020.pdf). Advances in Survival Analysis. International Biometric Society - British and Irish Region.

>August 2020 [A marginal model for relative survival.](pdf/IBC_PCL.pdf). Intenational Biometric Society 2020

>26 September 2019. [Issues in Standardization](pdf/Stockholm_workshop_Sept2019_Lambert.pdf). Symposium for statisticians working in register-based cancer epidemiology 2019, Stockholm, Sweden

>29 August 2019. [Standardised crude probabilities of death to improve understanding of national and international cancer survival comparisons](pdf/ANCR2019_Lambert.pdf). Association of the Nordic Cancer Registries meeting 2019, Stockholm Sweden

>30 August 2019. [Marginal estimates through regression standardization in competing risks and relative survival models](pdf/Stata_Nordic2019_Lambert.pdf). Nordic and Baltic Stata Users Group meeting 2019, Stockholm, Sweden.
