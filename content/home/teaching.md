+++
# A Recent and Upcoming Talks section created with the Pages widget.
# This section displays recent talks from `content/talk/`.

date = "2018-08-21"
draft = false

title = "Teaching"
subtitle = ""
widget = "custom"
math = true
# Order that this section will appear in.
weight = 60
+++
When I worked at the University of Leicester my main teaching was on the [MSc Medical Statisics](https://le.ac.uk/courses/medical-statistics-msc) course. I was a student on  this course way back in $1991 /  1992$.


I also teach specialist courses. Below are some upcoming course.

- 9 September 2024. [Modelling survival data using flexible parametric models in Stata using stpm3: concepts and modelling choices](https://www.kreftregisteret.no/Generelt/kalender/survival-2024/). Pre conference course at the [2024 Northern European Stata Conference](https://www.stata.com/meeting/northern-european24/) 

- June 2025. Paul Lambert, Paul Dickman, Mark Rutherford, Therese Andersson and Betty Syriopoulou aim to teach a 5-day course on statistical methods for population-based cancer survival analysis in Veneto Italy. Further details will be available [here](http://cansurv.net/). 


