+++
title = 'mrsprep'
date = '2020-12-24'
tags = ["software", "mrsprep"]

  
+++

The `mrsprep` command restructures survival data and calculates weighted mean expected mortality rates and time-dependent weights so that a marginal relative survival can be directly estimated. After running `mrsprep` estimation commands that fit (conditional) relative survival models (e.g. `stpm2` or `strcs`) can be used to estimate marginal relative survival without out the need to include covariates that affect expected survival. 

You can install `mrsprep` within Stata using

```stata
. ssc install mrsprep 
```

## Using `mrsprep`
- [Estimating marginal relative survival using `mrsprep` and `stpm2`.](/software/mrsprep/mrsprep_marginal_relative_survival/)
- [External age standardization.](/software/mrsprep/mrsprep_external_age_standardization/)
- [Modelling covariates.](/software/mrsprep/mrsprep_modelling_covariates/)

## Updates
See [mrsprep_releases.txt](/software/mrsprep/mrsprep_releases.txt)
