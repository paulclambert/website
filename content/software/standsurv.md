+++
title = 'standsurv'
date = '2019-03-28'
tags = ["software", "standsurv"]

  
+++

The `standsurv` command estimates standardized survival curves and related measures. It also allows various contrasts between the standardized functions. It is a post-estimation command and can be used after fitting a wide range of survival models. These include `streg` models (except generalized gamma), `stpm2` models and `strcs` models. 

Note I previously developed `stpm2_standsurv`, which only works with `stpm2` models, but `standsurv` superceeds that. You can do the same things with `standsurv` and much, much more.

So some of the examples are the same as `stpm2_standsurv`. I will add more examples soon showing some of the additional things `standsurv` can do.

You can install `standsurv` within Stata using

```stata
. ssc install standsurv 
```

Note that `standsurv` no longer available on this site, so please use SSC.


## Using `standsurv`

### Standard Survival Models
- [Standardized survival functions and contrasts.](/software/standsurv/standardized_survival/)
- [Centiles of the standardized survival function.](/software/standsurv/standardized_survival_centiles/)
- [Restricted mean survival using standardized survival functions.](/software/standsurv/standardized_survival_rmst/)
- [The hazard function of the standardized survival curve.](/software/standsurv/standardized_survival_hazard/)
- [Estimating attributable fractions in cohort studies](/software/standsurv/standardized_survival_AF/)
- [Some comments on why I am not so keen on `stteffects`](/software/standsurv/why_not_stteffects/)
- Why just not use `margins`?

### Competing Risk Models
- [Standardized cause-specific cumulative incidence functions.](/software/standsurv/standardized_cif/)
- Other useful standardized measures in competing risks

### Relative/Net Survival Models 
- [Standardized Relative Survival.](/software/standsurv/standardized_relative_survival/)  
- External age-standardization
- Loss in Expectation of Life
- [Standardized Crude Probabilities of death.](/software/standsurv/standardized_crude_probabilities_of_death) 

### General
- [Models fitted in different countries](/software/standsurv/models_different_countries)


## Releases
See [standsurv_releases.txt](/software/standsurv/standsurv_releases.txt)
