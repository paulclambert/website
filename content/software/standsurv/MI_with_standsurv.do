
set seed 89317
use https://www.pclambert.net/data/colon, clear

// stset with death from any cause
stset surv_mm, failure(status=1 2) scale(12) exit(time 10*12)

replace stage=. if stage==0
tab agegrp, gen(agegrp)
tab stage, gen(stage)

// compete case
// simple PH model
stpm2 stage2 stage3 agegrp2 agegrp3 agegrp4 , df(5) scale(hazard) eform nolog

// The cumulative hazard at the event time should be included in the imputation model.
sts gen H=na

// Declare multiple-imputation data and register variables
mi set flong
mi register imputed  stage
mi register regular subsite agegrp* sex
mi register passive _rcs* _d_rcs* stage1 stage2 stage3

// Perform imputation. 
// This creates 10 additional copies of the obs with missing stage.
// Better to have more datasets, for we use 10 for illustration
drop stage1 stage2 stage3
mi impute chained (mlogit) stage = i.subsite sex i.agegrp H _d, add(10)

// store number of imputed datasets           
mi query           
global M `r(M)'

// generate dummies
quietly mi xeq:  tab stage, gen(stage) 

// show stage distribution changes between imputed data sets
mi xeq 1 2: tab stage

// Note that as stpm2 is not an official Stata command we need to use cmd ok option
// Save the estimates so that they can be used for making predictions 
mi estimate, dots cmdok sav(mi_stpm2,replace): ///
    stpm2 stage2 stage3 agegrp2 agegrp3 agegrp4, df(5)  scale(hazard) nolog eform

// predict survival using -mi predictnl-	
mi predictnl survimp2 = predict(survival at(agegrp2 1) zeros timevar(_t)) using mi_stpm2

// compare predictions to complete case analysis
stpm2 stage2 stage3 agegrp2 agegrp3 agegrp4 if _mi_m==0, df(5) scale(hazard)
predict surv, survival at(agegrp 2) zeros

line surv survimp2 _t if stage==1 & _mi_m==0, sort || ///
line surv survimp2 _t if stage==2 & _mi_m==0, sort || ///
line surv survimp2 _t if stage==3 & _mi_m==0, sort ///
title("Predicted survival for agegrp==2 (60-74)") ///
legend(order(1 "Localised (Complete)" 2  "Localised (Imputed)" ///
              3 "Regional (Complete)" 4 "Regional (Imputed)" ///
			  5 "Distant (Complete)" 6 "Distant (Imputed)")) ///
           name(imputed, replace)


           
           
// now loop over datasets

mi xeq:  range tt 0 10 101

frame create standsurv_results,   
frame standsurv_results: range tt 0 10 101     
           
forvalues m = 1/$M {
	frame change default
	frame copy default tmpmi, replace
  
  frame change tmpmi
	mi extract `m', clear 
  estimates use mi_stpm2, number(`m')

  standsurv, timevar(tt) se atvar(ms)
  keep if tt!=.

  frame change standsurv_results 
 	frlink 1:1 tt, frame(tmpmi) 
  frget ms*, from(tmpmi) suffix(_`m')
  gen ms_var`m' = ms_se_`m'^2
  drop tmpmi
}           
 
frame change  standsurv_results

// Now apply Rubin's Rules
egen ms_all = rowmean(ms_? ms_??)

egen U = rowmean(ms_var*)
egen sqrtB = rowsd(ms_var*)
gen ms_all_se = sqrt(U + (1+1/${M})*sqrtB^2)

gen ms_all_lci = ms_all - 1.96*ms_all_se
gen ms_all_uci = ms_all + 1.96*ms_all_se

twoway line ms_all ms_all_lci ms_all_uci tt  