+++
title = 'stpm2'
date = '2017-08-21'
tags = ["software", "stpm2"]

  
+++

`stpm2` fits flexible parametric survival models. These models use splines to model some transformation of the survial function. The most common is the  $\log\[-\log\[S(t)\]\]$ link function, which fits proportional hazards models.

I have added some examples and aim to add to these.

## Proportional hazards models
- [Comparison with a Cox model](/software/stpm2/comparewithcox/)
- Simple simulation study to show agreement with Cox model.
- [Predicting hazard and survival functions (use of the `timevar()` option)](/software/stpm2/stpm2_timevar/)
- [Sensitivity analysis for the number of knots](/software/stpm2/sensitivity_analysis/).
- [The default knot positions - are they sensible?](/software/stpm2/knot_positions_sensitivity/)
- [Out of sample predictions (by Sarah Booth)](/software/stpm2/out_of_sample_predictions/)

## Time-dependent effects (non proportional hazards)
- Non-proportional hazards

## Prognostic Models
- [Temporal Recalibration (by Sarah Booth)](/software/stpm2/temporal_recalibration/)

## Relative survival 
- A simple relative survival model

## Alternative link functions
- the logistic, probit and Aranda-Ordaz link functions.



