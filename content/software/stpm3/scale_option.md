+++
date = "2022-09-03"
title = "Change of scale option"
summary = "stpm3"
tags = ["stpm3","stpm2","survival","software","Stata"]
external_link = "" 
math = true
[header]
image = ""
caption = ""
+++

# The scale option has changed

In `stpm2` the choice of options when using the `scale()` option included `hazard` and `odds`.
These really meant log cumulative hazard and log odds respectively.

In `stpm3` you need to refer to these models as `scale(lncumhazard)` or
`scale(lnodds)`. This is because `stpm3` allows models to be fitted on 
the log hazard scale (`scale(lnhazard)`) and thus there is a need to distinguish between models
on the log cumulative hazard and log hazard scales.


We first load the example Rotterdam 2 breast cancer data and then use `stset` to declare the survival time and event indicator.

```stata
. use https://www.pclambert.net/data/rott2b, clear
(Rotterdam breast cancer data (augmented with cause of death))

. stset os, f(osi==1) scale(12) exit(time 120)

Survival-time data settings

         Failure event: osi==1
Observed time interval: (0, os]
     Exit on or before: time 120
     Time for analysis: time/12

--------------------------------------------------------------------------
      2,982  total observations
          0  exclusions
--------------------------------------------------------------------------
      2,982  observations remaining, representing
      1,171  failures in single-record/single-failure data
 20,002.424  total analysis time at risk and under observation
                                                At risk from t =         0
                                     Earliest observed entry t =         0
                                          Last observed exit t =        10

```

The `scale(12)` option converts the times recorded in months to years.

To fit an `stpm2` model we would use,

```stata
. stpm2 hormon, scale(hazard) df(5) 

Iteration 0:   log likelihood = -2929.2995  
Iteration 1:   log likelihood = -2928.2998  
Iteration 2:   log likelihood = -2928.2966  
Iteration 3:   log likelihood = -2928.2966  

Log likelihood = -2928.2966                              Number of obs = 2,982

------------------------------------------------------------------------------
             | Coefficient  Std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
xb           |
      hormon |   .4321954   .0861189     5.02   0.000     .2634054    .6009854
       _rcs1 |    .914158   .0273548    33.42   0.000     .8605436    .9677725
       _rcs2 |   .1672938   .0273886     6.11   0.000     .1136131    .2209746
       _rcs3 |   .0360402   .0154393     2.33   0.020     .0057797    .0663006
       _rcs4 |  -.0113638    .007644    -1.49   0.137    -.0263459    .0036182
       _rcs5 |   .0058281   .0049328     1.18   0.237    -.0038401    .0154963
       _cons |  -1.225443   .0332548   -36.85   0.000    -1.290622   -1.160265
------------------------------------------------------------------------------

```

The equivalent model in `stpm3` is, 

```stata
. stpm3 hormon, scale(lncumhazard) df(5) 

Iteration 0:   log likelihood = -2929.2941  
Iteration 1:   log likelihood = -2928.2998  
Iteration 2:   log likelihood = -2928.2966  
Iteration 3:   log likelihood = -2928.2966  

                                                        Number of obs =  2,982
                                                        Wald chi2(1)  =  25.19
Log likelihood = -2928.2966                             Prob > chi2   = 0.0000

------------------------------------------------------------------------------
             | Coefficient  Std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
xb           |
      hormon |   .4321954   .0861189     5.02   0.000     .2634054    .6009854
-------------+----------------------------------------------------------------
time         |
        _ns1 |   -23.9834    1.92113   -12.48   0.000    -27.74874   -20.21805
        _ns2 |   6.695765   1.027919     6.51   0.000     4.681082    8.710449
        _ns3 |  -1.214676   .0497438   -24.42   0.000    -1.312172    -1.11718
        _ns4 |  -.8095755   .0387379   -20.90   0.000    -.8855004   -.7336505
        _ns5 |  -.4994385   .0418591   -11.93   0.000    -.5814808   -.4173963
       _cons |  -.5713643   .0332128   -17.20   0.000    -.6364603   -.5062684
------------------------------------------------------------------------------
Warning: This is a test version of stpm3

```

Note the log-likelihoods are identical as are the coefficients/standard errors for `hormon`.
Note that different basis functions are used, so the coefficients for the spline terms
are different. However, predicted values for the same covariate pattern will not differ.


To fit a model on the log hazard scale use `scale(lnhazard)`,

```stata
. stpm3 hormon, scale(lnhazard) df(5) 

Iteration 0:   log likelihood = -14911.675  
Iteration 1:   log likelihood = -3026.8801  
Iteration 2:   log likelihood = -2948.3778  
Iteration 3:   log likelihood =  -2932.594  
Iteration 4:   log likelihood = -2930.2148  
Iteration 5:   log likelihood = -2930.1543  
Iteration 6:   log likelihood = -2930.1543  

                                                        Number of obs =  2,982
                                                        Wald chi2(1)  =  25.25
Log likelihood = -2930.1543                             Prob > chi2   = 0.0000

------------------------------------------------------------------------------
             | Coefficient  Std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
xb           |
      hormon |   .4327572   .0861241     5.02   0.000      .263957    .6015574
-------------+----------------------------------------------------------------
time         |
        _ns1 |  -8.932832   1.582843    -5.64   0.000    -12.03515   -5.830516
        _ns2 |   4.310232   .9613182     4.48   0.000     2.426083    6.194381
        _ns3 |   .3888339   .1855177     2.10   0.036     .0252258    .7524419
        _ns4 |   .1035977   .1619785     0.64   0.522    -.2138744    .4210698
        _ns5 |   .0857984   .3383806     0.25   0.800    -.5774153    .7490121
       _cons |  -2.911815    .146526   -19.87   0.000       -3.199   -2.624629
------------------------------------------------------------------------------
Quadrature method: Gauss-Legendre with 30 nodes
Warning: This is a test version of stpm3

```


