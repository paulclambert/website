## Run this to get final references in bold before uploading to website

#install.packages('blogdown')
#blogdown::install_hugo()

library(rmarkdown)

## First copy allrefs.bib to folder
## Note on MEB laptop stored in github folder
file.copy(from="../survbib/allrefs.bib",
            to="pcl/allpublications/allrefs.bib",
            overwrite=TRUE)

## Run allrefs.Rmd to convert to markdown
rmarkdown::render("pcl/allpublications/allrefs.Rmd")

## Due to problem with citation order I now remove the first few 
## lines 

## remove line breaks from references
inputtext <- readLines("pcl/allpublications/allrefs.md")
newlinenum <- 1 
outputtext <- ""
for (i in 6:length(inputtext)) { 
  currentline <- inputtext[i]
  if(currentline != "") {
    outputtext[newlinenum] <- paste(outputtext[newlinenum], currentline,sep= " ")
  } 
  else {
    newlinenum <- newlinenum + 1
    outputtext[newlinenum] <- ""
    outputtext[newlinenum] <- currentline
    newlinenum <- newlinenum + 1     
    outputtext[newlinenum] <- ""
  }
}

##  Convert Lambert PC to **Lambert PC**
outputtext <- gsub( "Lambert PC", "**Lambert PC**", outputtext )
cat(outputtext, file="pcl/allpublications/allrefs.md", sep="\n")

## Append files to final markdown file
file.remove("content/allpublications/_index.md")
x <- readLines("pcl/allpublications/header.md")
cat(x, file="content/allpublications/_index.md", sep="\n")
x <- readLines("pcl/allpublications/allrefs.md")
cat(x, file="content/allpublications/_index.md", sep="\n", append=TRUE)

## Tidy up
rm(newlinenum,i,inputtext,outputtext,currentline)

