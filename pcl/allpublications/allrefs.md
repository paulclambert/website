------------------------------------------------------------------------

nocite:
|<sup>[1](#ref-Johansson2024)</sup>;<sup>[2](#ref-Lundberg2024)</sup>;<sup>[3](#ref-Hall2024)</sup>;<sup>[**Lee2024?**](#ref-Lee2024)</sup>;<sup>[**Jennings2023?**](#ref-Jennings2023)</sup>;<sup>[**Leontyeva2023?**](#ref-Leontyeva2023)</sup>;<sup>[**Waterhouse2023?**](#ref-Waterhouse2023)</sup>;<sup>[**Booth2023?**](#ref-Booth2023)</sup>;<sup>[**Tyrer2023?**](#ref-Tyrer2023)</sup>;<sup>[**Wells2023?**](#ref-Wells2023)</sup>;<sup>[**Sweeting2023?**](#ref-Sweeting2023)</sup>;<sup>[**Myklebust2023?**](#ref-Myklebust2023)</sup>;<sup>[**Teece2023?**](#ref-Teece2023)</sup>;<sup>[**Skourlis2023?**](#ref-Skourlis2023)</sup>;<sup>[**Coles2023?**](#ref-Coles2023)</sup>;<sup>[4](#ref-Lundberg2022a)</sup>;<sup>[5](#ref-Batyrbekova2022)</sup>;<sup>[6](#ref-Syriopoulou2022a)</sup>;<sup>[7](#ref-Leontyeva2022)</sup>;<sup>[8](#ref-Syriopoulou2022)</sup>;<sup>[9](#ref-Schmidt2022)</sup>;<sup>[10](#ref-Lundberg2022)</sup>;<sup>[**Bower2022?**](#ref-Bower2022)</sup>;<sup>[11](#ref-Smith2022)</sup>;<sup>[12](#ref-Stannard2022)</sup>;<sup>[13](#ref-Skourlis2022)</sup>;<sup>[14](#ref-Andersson2022b)</sup>;<sup>[7](#ref-Leontyeva2022)</sup>;<sup>[15](#ref-Andersson2022a)</sup>;<sup>[16](#ref-Rutherford2022)</sup>;<sup>[17](#ref-Andersson2022)</sup>;<sup>[18](#ref-Lee2022)</sup>;<sup>[19](#ref-Riley2021)</sup>;<sup>[20](#ref-Skourlis2021)</sup>;<sup>[21](#ref-Syriopoulou2021a)</sup>;<sup>[22](#ref-Sweeting2021)</sup>;<sup>[23](#ref-Coles2021)</sup>;<sup>[24](#ref-Lambert2021)</sup>;<sup>[25](#ref-Mozumder2021)</sup>;<sup>[26](#ref-Ensor2021)</sup>;<sup>[27](#ref-Hill2021)</sup>;<sup>[28](#ref-Weibull2021)</sup>;<sup>[29](#ref-Andersson2021)</sup>;<sup>[30](#ref-TSD21)</sup>;<sup>[31](#ref-Andersson2021a)</sup>;<sup>[32](#ref-Syriopoulou2021)</sup>;<sup>[33](#ref-Smith2020)</sup>;<sup>[34](#ref-Lundberg2020)</sup>;<sup>[35](#ref-Lambert2020)</sup>;<sup>[36](#ref-Myklebust2020)</sup>;<sup>[37](#ref-Rutherford2020)</sup>;<sup>[38](#ref-Welch2020)</sup>;<sup>[39](#ref-Booth2020)</sup>;<sup>[40](#ref-Syriopoulou2020)</sup>;<sup>[41](#ref-Arnold2019)</sup>;<sup>[42](#ref-Wong2019)</sup>;<sup>[43](#ref-Andersson2019)</sup>;<sup>[44](#ref-Bower2019a)</sup>;<sup>[45](#ref-Syriopoulou2019)</sup>;<sup>[46](#ref-Rutherford2019)</sup>;<sup>[47](#ref-Bower2019)</sup>;<sup>[48](#ref-Weibull2019)</sup>;<sup>[49](#ref-Syriopoulou2018)</sup>;<sup>[50](#ref-Plym2018)</sup>;<sup>[51](#ref-Mozumder2018)</sup>;<sup>[52](#ref-Weibull2018)</sup>;<sup>[53](#ref-Latimer2018)</sup>;<sup>[54](#ref-Mozumder2017)</sup>;<sup>[55](#ref-Mozumder2016)</sup>;<sup>[56](#ref-Bower2017)</sup>;<sup>[**Latimer2017?**](#ref-Latimer2017)</sup>;<sup>[57](#ref-Peters2017)</sup>;<sup>[58](#ref-Crowther2016a)</sup>;<sup>[59](#ref-Cramb2016)</sup>;<sup>[60](#ref-Lambert2016a)</sup>;<sup>[61](#ref-Lambert2016)</sup>;<sup>[62](#ref-Syriopoulou2017)</sup>;<sup>[63](#ref-Bower2016a)</sup>;<sup>[64](#ref-Bower2016b)</sup>;<sup>[**Bower2016c?**](#ref-Bower2016c)</sup>;<sup>[65](#ref-Edgren2016)</sup>;<sup>[66](#ref-Crowther2016)</sup>;<sup>[67](#ref-Andersson2015)</sup>;<sup>[68](#ref-Hultcrantz2015)</sup>;<sup>[**Rutherford2015d?**](#ref-Rutherford2015d)</sup>;<sup>[69](#ref-Rutherford2015c)</sup>;<sup>[70](#ref-Crowther2015)</sup>;<sup>[71](#ref-Lambert2015)</sup>;<sup>[72](#ref-Rutherford2015b)</sup>;<sup>[73](#ref-Rutherford2013c)</sup>;<sup>[74](#ref-Crowther2014)</sup>;<sup>[75](#ref-Eloranta2014)</sup>;<sup>[76](#ref-Feldman2014)</sup>;<sup>[77](#ref-Gao2014)</sup>;<sup>[78](#ref-Latimer2014)</sup>;<sup>[79](#ref-Andersson2014)</sup>;<sup>[80](#ref-Andersson2013)</sup>;<sup>[81](#ref-Ahyow2013)</sup>;<sup>[82](#ref-Crowther2013a)</sup>;<sup>[83](#ref-Crowther2013)</sup>;<sup>[84](#ref-Crowther2013c)</sup>;<sup>[85](#ref-Crowther2013b)</sup>;<sup>[86](#ref-Cvancarova2013)</sup>;<sup>[87](#ref-Eloranta2013)</sup>;<sup>[88](#ref-Eloranta2013a)</sup>;<sup>[89](#ref-Hinchliffe2013b)</sup>;<sup>[90](#ref-Hinchliffe2013)</sup>;<sup>[91](#ref-Hinchliffe2013d)</sup>;<sup>[92](#ref-Hinchliffe2013e)</sup>;<sup>[93](#ref-Hinchliffe2013a)</sup>;<sup>[94](#ref-Rutherford2013b)</sup>;<sup>[95](#ref-Rutherford2013a)</sup>;<sup>[96](#ref-Dickman2013)</sup>;<sup>[97](#ref-Shah2013)</sup>;<sup>[98](#ref-Yu2013)</sup>;<sup>[99](#ref-Andersson2012)</sup>;<sup>[100](#ref-Andrae2012)</sup>;<sup>[101](#ref-Crowther2012)</sup>;<sup>[102](#ref-Crowther2012b)</sup>;<sup>[103](#ref-Crowther2012a)</sup>;<sup>[104](#ref-Eloranta2012)</sup>;<sup>[105](#ref-Hinchliffe2012)</sup>;<sup>[106](#ref-Hinchliffe2012a)</sup>;<sup>[107](#ref-Holmberg2012)</sup>;<sup>[108](#ref-Moeller2012)</sup>;<sup>[109](#ref-Rutherford2012)</sup>;<sup>[110](#ref-Rutherford2012a)</sup>;<sup>[111](#ref-Shack2012)</sup>;<sup>[112](#ref-Andersson2011)</sup>;<sup>[113](#ref-Eaker2011)</sup>;<sup>[114](#ref-Hakulinen2011)</sup>;<sup>[115](#ref-Lambert2011)</sup>;<sup>[116](#ref-Coleman2011a)</sup>;<sup>[117](#ref-Morden2011)</sup>;<sup>[118](#ref-Morris2011)</sup>;<sup>[119](#ref-Andersson2010)</sup>;<sup>[120](#ref-Eloranta2010)</sup>;<sup>[121](#ref-Lambert2010a)</sup>;<sup>[122](#ref-Lambert2010)</sup>;<sup>[123](#ref-Riley2010)</sup>;<sup>[124](#ref-Rutherford2010)</sup>;<sup>[125](#ref-Squire2010)</sup>;<sup>[126](#ref-Lambert2009)</sup>;<sup>[127](#ref-Larfors2009)</sup>;<sup>[128](#ref-Panickar2009)</sup>;<sup>[129](#ref-Woods2009)</sup>;<sup>[130](#ref-Bhaskaran2008)</sup>;<sup>[131](#ref-Gillies2008)</sup>;<sup>[132](#ref-Lambert2008a)</sup>;<sup>[133](#ref-Nelson2008)</sup>;<sup>[134](#ref-Lambert2008)</sup>;<sup>[135](#ref-Reynolds2008)</sup>;<sup>[136](#ref-Riley2008)</sup>;<sup>[137](#ref-Cooper2007)</sup>;<sup>[138](#ref-Gillies2007)</sup>;<sup>[139](#ref-Lambert2007b)</sup>;<sup>[140](#ref-Lambert2007)</sup>;<sup>[141](#ref-Lambert2007a)</sup>;<sup>[142](#ref-Manca2007)</sup>;<sup>[143](#ref-Nelson2007)</sup>;<sup>[144](#ref-Riley2007a)</sup>;<sup>[145](#ref-Riley2007)</sup>;<sup>[146](#ref-Sutton2007)</sup>;<sup>[147](#ref-Abrams2005)</sup>;<sup>[148](#ref-Lambert2005)</sup>;<sup>[149](#ref-Lambert2005a)</sup>;<sup>[150](#ref-Minelli2005)</sup>;<sup>[151](#ref-Sutton2005)</sup>;<sup>[152](#ref-Taub2005)</sup>;<sup>[153](#ref-Lambert2004)</sup>;<sup>[154](#ref-Riley2004a)</sup>;<sup>[155](#ref-Riley2004)</sup>;<sup>[156](#ref-Smith2004)</sup>;<sup>[157](#ref-Sweeting2004)</sup>;<sup>[158](#ref-Baker2003)</sup>;<sup>[159](#ref-Hsu2003)</sup>;<sup>[160](#ref-Mckean2003)</sup>;<sup>[161](#ref-Oommen2003)</sup>;<sup>[158](#ref-Baker2003)</sup>;<sup>[162](#ref-Riley2003)</sup>;<sup>[**Cooper2003?**](#ref-Cooper2003)</sup>;<sup>[163](#ref-Riley2003a)</sup>;<sup>[**Waugh2003b?**](#ref-Waugh2003b)</sup>;<sup>[164](#ref-Riley2003b)</sup>;<sup>[165](#ref-Smith2003)</sup>;<sup>[166](#ref-Waugh2003)</sup>;<sup>[167](#ref-Lambert2002)</sup>;<sup>[168](#ref-Young2002)</sup>;<sup>[**Sutton2002?**](#ref-Sutton2002)</sup>;<sup>[169](#ref-Lambert2001)</sup>;<sup>[170](#ref-Mckean2001)</sup>;<sup>[171](#ref-Waugh2000)</sup>;<sup>[172](#ref-Bell1999)</sup>;<sup>[173](#ref-Williams1999)</sup>;<sup>[174](#ref-Brooke1998)</sup>;<sup>[175](#ref-Hellmich1998)</sup>;<sup>[176](#ref-Penny1998)</sup>;<sup>[177](#ref-Halligan1997)</sup>;<sup>[178](#ref-Brooke1996)</sup>;<sup>[179](#ref-Halligan1996)</sup>;<sup>[180](#ref-Halligan1996a)</sup>;<sup>[181](#ref-Peek1996)</sup>;<sup>[**Luyt1995?**](#ref-Luyt1995)</sup>;<sup>[182](#ref-Brooke1995)</sup>;<sup>[183](#ref-Esmail1995)</sup>

Last updated 2024-08-18

<span class="csl-left-margin">Johansson ALV, Kønig SM, Larønningen S,
Engholm G, Kroman N, Seppä K, Malila N, Steig BÁ, Gudmundsdóttir EM,
Ólafsdóttir EJ, Lundberg FE, Andersson TM-L, Lambert PC, Lambe M,
Pettersson D, Aagnes B, Friis S, Storm H.
</span><span class="csl-right-inline">Have the recent advancements in
cancer therapy and survival benefitted patients of all age groups across
the Nordic countries? NORDCAN survival analyses 2002-2021. *Acta
Oncologica* 2024;**63**:179–91.
<https://doi.org/10.2340/1651-226X.2024.35094>.</span>

<span class="csl-left-margin">Lundberg FE, Birgisson H, Engholm G,
Ólafsdóttir EJ, Mørch LS, Johannesen TB, Pettersson D, Lambe M, Seppä K,
Lambert PC, Johansson ALV, Hölmich LR, Andersson TM-L.
</span><span class="csl-right-inline">Survival trends for patients
diagnosed with cutaneous malignant melanoma in the Nordic countries
1990-2016: The NORDCAN survival studies. *European Journal of Cancer*
2024;**202**:113980.
<https://doi.org/10.1016/j.ejca.2024.113980>.</span>

<span class="csl-left-margin">Hall M, Smith L, Wu J, Hayward C, Batty
JA, Lambert PC, Hemingway H, Gale CP.
</span><span class="csl-right-inline">Health outcomes after myocardial
infarction: A population study of 56 million people in England. *PLoS
Medicine* 2024;**21**:e1004343.
<https://doi.org/10.1371/journal.pmed.1004343>.</span>

<span class="csl-left-margin">Lundberg FE, Kroman N, Lambe M, Andersson
TM-L, Engholm G, Johannesen TB, Virtanen A, Pettersson D, Ólafsdóttir
EJ, Birgisson H, Lambert PC, Mørch LS, Johansson ALV.
</span><span class="csl-right-inline">Age-specific survival trends and
life-years lost in women with breast cancer 1990-2016: The NORDCAN
survival studies. *Acta Oncologica* 2022;**61**:1481–9.
<https://doi.org/10.1080/0284186X.2022.2156811>.</span>

<span class="csl-left-margin">Batyrbekova N, Bower H, Dickman PW, Ravn
Landtblom A, Hultcrantz M, Szulkin R, Lambert PC, Andersson TM-L.
</span><span class="csl-right-inline">Modelling multiple time-scales
with flexible parametric survival models. *BMC Medical Research
Methodology* 2022;**22**:290.
<https://doi.org/10.1186/s12874-022-01773-9>.</span>

<span class="csl-left-margin">Syriopoulou E, Wästerlid T, Lambert PC,
Andersson TM-L. </span><span class="csl-right-inline">Standardised
survival probabilities: A useful and informative tool for reporting
regression models for survival data. *British Journal of Cancer*
2022;**127**:1808–15.
<https://doi.org/10.1038/s41416-022-01949-6>.</span>

<span class="csl-left-margin">Leontyeva Y, Bower H, Gauffin O, Lambert
PC, Andersson TM-L. </span><span class="csl-right-inline">Assessing the
impact of including variation in general population mortality on
standard errors of relative survival and loss in life expectancy. *BMC
Medical Research Methodology* 2022;**22**:130.
<https://doi.org/10.1186/s12874-022-01597-7>.</span>

<span class="csl-left-margin">Syriopoulou E, Mozumder SI, Rutherford MJ,
Lambert PC. </span><span class="csl-right-inline">Estimating causal
effects in the presence of competing events using regression
standardisation with the stata command
<span class="nocase">s</span>tandsurv. *BMC Medical Research
Methodology* 2022;**22**:226.
<https://doi.org/10.1186/s12874-022-01666-x>.</span>

<span class="csl-left-margin">Schmidt JCF, Lambert PC, Gillies CL,
Sweeting MJ. </span><span class="csl-right-inline">Patterns of rates of
mortality in the clinical practice research datalink. *PloS One*
2022;**17**:e0265709.
<https://doi.org/10.1371/journal.pone.0265709>.</span>

<span class="csl-left-margin">Lundberg FE, Birgisson H, Johannesen TB,
Engholm G, Virtanen A, Pettersson D, Ólafsdóttir EJ, Lambe M, Lambert
PC, Mørch LS, Johansson ALV, Andersson TM-L.
</span><span class="csl-right-inline">Survival trends in patients
diagnosed with colon and rectal cancer in the nordic countries
1990-2016: The NORDCAN survival studies. *European Journal of Cancer
(Oxford, England : 1990)* 2022;**172**:76–84.
<https://doi.org/10.1016/j.ejca.2022.05.032>.</span>

<span class="csl-left-margin">Smith A, Lambert PC, Rutherford MJ.
</span><span class="csl-right-inline">Generating high-fidelity synthetic
time-to-event datasets to improve data transparency and accessibility.
*BMC Medical Research Methodology* 2022;**22**:176.
<https://doi.org/10.1186/s12874-022-01654-1>.</span>

<span class="csl-left-margin">Stannard R, Lambert PC, Andersson TM-L,
Rutherford MJ. </span><span class="csl-right-inline">Obtaining long-term
stage-specific relative survival estimates in the presence of incomplete
historical stage information. *British Journal of Cancer*
2022;**127**:1061–8.
<https://doi.org/10.1038/s41416-022-01866-8>.</span>

<span class="csl-left-margin">Skourlis N, Crowther MJ, Andersson TM-L,
Lambert PC. </span><span class="csl-right-inline">On the choice of
timescale for other cause mortality in a competing risk setting using
flexible parametric survival models. *Biometrical Journal Biometrische
Zeitschrift* 2022;**64**:1161–77.
<https://doi.org/10.1002/bimj.202100254>.</span>

<span class="csl-left-margin">Andersson TM-L, Rutherford MJ, Møller B,
Lambert PC, Myklebust TÅ.
</span><span class="csl-right-inline">Reference-adjusted loss in life
expectancy for population-based cancer patient survival comparisons-with
an application to colon cancer in sweden. *Cancer Epidemiology,
Biomarkers & Prevention : A Publication of the American Association for
Cancer Research, Cosponsored by the American Society of Preventive
Oncology* 2022;**31**:1720–6.
<https://doi.org/10.1158/1055-9965.EPI-22-0137>.</span>

<span class="csl-left-margin">Andersson TM-L, Myklebust TÅ, Rutherford
MJ, Møller B, Arnold M, Soerjomataram I, Bray F, Parkin DM, Lambert PC.
</span><span class="csl-right-inline">Five ways to improve international
comparisons of cancer survival: Lessons learned from ICBP SURVMARK-2.
*British Journal of Cancer* 2022;**126**:1224–8.
<https://doi.org/10.1038/s41416-022-01701-0>.</span>

<span class="csl-left-margin">Rutherford MJ, Andersson TM-L, Myklebust
TÅ, Møller B, Lambert PC.
</span><span class="csl-right-inline">Non-parametric estimation of
reference adjusted, standardised probabilities of all-cause death and
death due to cancer for population group comparisons. *BMC Medical
Research Methodology* 2022;**22**:2.
<https://doi.org/10.1186/s12874-021-01465-w>.</span>

<span class="csl-left-margin">Andersson TM-L, Rutherford MJ, Myklebust
TÅ, Møller B, Arnold M, Soerjomataram I, Bray F, Elkader HA, Engholm G,
Huws D, Little A, Shack L, Walsh PM, Woods RR, Parkin DM, Lambert PC.
</span><span class="csl-right-inline">A way to explore the existence of
‘immortals’ in cancer registry data - an illustration using data from
ICBP SURVMARK-2. *Cancer Epidemiology* 2022;**76**:102085.
<https://doi.org/10.1016/j.canep.2021.102085>.</span>

<span class="csl-left-margin">Lee SF, Vellayappan BA, Wong LC, Chiang
CL, Chan SK, Wan EY-F, Wong IC-K, Lambert PC, Rachet B, Ng AK,
Luque-Fernandez MA. </span><span class="csl-right-inline">Cardiovascular
diseases among diffuse large b-cell lymphoma long-term survivors in
asia: A multistate model study. *ESMO Open* 2022;**7**:100363.
<https://doi.org/10.1016/j.esmoop.2021.100363>.</span>

<span class="csl-left-margin">Riley RD, Collins GS, Ensor J, Archer L,
Booth S, Mozumder SI, Rutherford MJ, Smeden M van, Lambert PC, Snell
KIE. </span><span class="csl-right-inline">Minimum sample size
calculations for external validation of a clinical prediction model with
a time-to-event outcome. *Statistics in Medicine* 2021;**41**:1280–95.
<https://doi.org/10.1002/sim.9275>.</span>

<span class="csl-left-margin">Skourlis N, Crowther MJ, Andersson TM-L,
Lambert PC. </span><span class="csl-right-inline">Development of a
dynamic interactive web tool to enhance understanding of multi-state
model analyses: MSMplus. *BMC Medical Research Methodology*
2021;**21**:262. <https://doi.org/10.1186/s12874-021-01420-9>.</span>

<span class="csl-left-margin">Syriopoulou E, Rutherford MJ, Lambert PC.
</span><span class="csl-right-inline">Inverse probability weighting and
doubly robust standardization in the relative survival framework.
*Statistics in Medicine* 2021.
<https://doi.org/10.1002/sim.9171>.</span>

<span class="csl-left-margin">Sweeting MJ, Oliver-Williams C, Teece L,
Welch CA, Belder MA de, Coles B, Lambert PC, Paley L, Rutherford MJ,
Elliss-Brookes L, Deanfield J, Peake MD, Adlam D.
</span><span class="csl-right-inline">Data resource profile: The Virtual
Cardio-Oncology Research Initiative (VICORI) linking national English
cancer registration and cardiovascular audits. *International Journal of
Epidemiology* 2022;**50**:1768–79.
<https://doi.org/10.1093/ije/dyab082>.</span>

<span class="csl-left-margin">Coles B, Teece L, Weston C, Belder MA de,
Oliver-Williams C, Welch CA, Rutherford MJ, Lambert PC, Bidulka P, Paley
L, Nitsch D, Deanfield J, Peake MD, Adlam D, Sweeting MJ, collaborative
V. </span><span class="csl-right-inline">Case-ascertainment of acute
myocardial infarction hospitalisations in cancer patients: A cohort
study using English linked electronic health data. *European Heart
Journal Quality of Care & Clinical Outcomes* 2021;**8**:86–95.
<https://doi.org/10.1093/ehjqcco/qcab045>.</span>

<span class="csl-left-margin">Lambert PC, Syriopoulou E, Rutherford MJ.
</span><span class="csl-right-inline">Direct modelling of age
standardized marginal relative survival through incorporation of
time-dependent weights. *BMC Medical Research Methodology*
2021;**21**:84. <https://doi.org/10.1186/s12874-021-01266-1>.</span>

<span class="csl-left-margin">Mozumder SI, Rutherford MJ, Lambert PC.
</span><span class="csl-right-inline">Estimating restricted mean
survival time and expected life-years lost in the presence of competing
risks within flexible parametric survival models. *BMC Medical Research
Methodology* 2021;**21**:52.
<https://doi.org/10.1186/s12874-021-01213-0>.</span>

<span class="csl-left-margin">Ensor J, Snell KIE, Debray TPA, Lambert
PC, Look MP, Mamas MA, Moons KGM, Riley RD.
</span><span class="csl-right-inline">Individual participant data
meta-analysis for external validation, recalibration, and updating of a
flexible parametric prognostic model. *Statistics in Medicine*
2021;**40**:3066–84. <https://doi.org/10.1002/sim.8959>.</span>

<span class="csl-left-margin">Hill M, Lambert PC, Crowther MJ.
</span><span class="csl-right-inline">Relaxing the assumption of
constant transition rates in a multi-state model in hospital
epidemiology. *BMC Medical Research Methodology* 2021;**21**:16.
<https://doi.org/10.1186/s12874-020-01192-8>.</span>

<span class="csl-left-margin">Weibull CE, Lambert PC, Eloranta S,
Andersson TM-L, Dickman PW, Crowther MJ.
</span><span class="csl-right-inline">A multistate model incorporating
estimation of excess hazards and multiple time scales. *Statistics in
Medicine* 2021;**40**:2139–54.
<https://doi.org/10.1002/sim.8894>.</span>

<span class="csl-left-margin">Andersson TM-L, Rutherford MJ, Myklebust
TÅ, Møller B, Soerjomataram I, Arnold M, Bray F, Parkin DM, Sasieni P,
Bucher O, De P, Engholm G, Gavin A, Little A, Porter G, Ramanakumar AV,
Saint-Jacques N, Walsh PM, Woods RR, Lambert PC.
</span><span class="csl-right-inline">Exploring the impact of cancer
registry completeness on international cancer survival differences: A
simulation study. *British Journal of Cancer* 2021;**124**:1026–32.
<https://doi.org/10.1038/s41416-020-01196-7>.</span>

<span class="csl-left-margin">Rutherford MJ, Lambert PC, Sweeting MJ,
Pennington B, Crowther MJ, Abrams KR, Latime NR.
</span><span class="csl-right-inline">NICE DSU TECHNICAL SUPPORT
DOCUMENT 21: Flexible methods for survival analysis. *Decision Support
Unit, University of Sheffield* 2021.</span>

<span class="csl-left-margin">Andersson TM-L, Myklebust TÅ, Rutherford
MJ, Møller B, Soerjomataram I, Arnold M, Bray F, Parkin DM, Sasieni P,
Bucher O, De P, Engholm G, Gavin A, Little A, Porter G, Ramanakumar AV,
Saint-Jacques N, Walsh PM, Woods RR, Lambert PC.
</span><span class="csl-right-inline">The impact of excluding or
including Death Certificate Initiated (DCI) cases on estimated cancer
survival: A simulation study. *Cancer Epidemiology* 2021;**71**:101881.
<https://doi.org/10.1016/j.canep.2020.101881>.</span>

<span class="csl-left-margin">Syriopoulou E, Rutherford MJ, Lambert PC.
</span><span class="csl-right-inline">Understanding disparities in
cancer prognosis: An extension of mediation analysis to the relative
survival framework. *Biometrical Journal* 2021;**63**:341–53.
<https://doi.org/10.1002/bimj.201900355>.</span>

<span class="csl-left-margin">Smith AJ, Lambert PC, Rutherford MJ.
</span><span class="csl-right-inline">Understanding the impact of sex
and stage differences on melanoma cancer patient survival: A SEER-based
study. *British Journal of Cancer* 2021;**1124**:671–7.
<https://doi.org/10.1038/s41416-020-01144-5>.</span>

<span class="csl-left-margin">Lundberg FE, Andersson TM-L, Lambe M,
Engholm G, Mørch LS, Johannesen TB, Virtanen A, Pettersson D,
Olafsdottir EJ, Birgisson H, Johansson ALV, Lambert PC.
</span><span class="csl-right-inline">Trends in cancer survival in the
Nordic countries 1990-2016: The NORDCAN survival studies. *Acta
Oncologica* 2020;**59**:1266–74.
<https://doi.org/10.1080/0284186X.2020.1822544>.</span>

<span class="csl-left-margin">Lambert PC, Andersson TM-L, Rutherford MJ,
Myklebust TÅ, Møller B.
</span><span class="csl-right-inline">Reference-adjusted and
standardized all-cause and crude probabilities as an alternative to net
survival in population-based cancer studies. *International Journal of
Epidemiology* 2020;**49**:1614–23.
<https://doi.org/10.1093/ije/dyaa112>.</span>

<span class="csl-left-margin">Myklebust TÅ, Andersson TM-L, Bardot A,
Vernon S, Gavin A, Fitzpatrick D, Jerm MB, Rutherford MJ, Parkin DM,
Sasieni P, Arnold M, Soerjomataram I, Bray F, Lambert PC, Møller B.
</span><span class="csl-right-inline">Can different definitions of date
of cancer incidence explain observed international variation in cancer
survival? An ICBP SURVMARK-2 study. *Cancer Epidemiology*
2020;**67**:101759.
<https://doi.org/10.1016/j.canep.2020.101759>.</span>

<span class="csl-left-margin">Rutherford MJ, Dickman PW, Coviello E,
Lambert PC. </span><span class="csl-right-inline">Estimation of
age-standardized net survival, even when age-specific data are sparse.
*Cancer Epidemiology* 2020;**67**:101745.
<https://doi.org/10.1016/j.canep.2020.101745>.</span>

<span class="csl-left-margin">Welch CA, Sweeting MJ, Lambert PC,
Rutherford MJ, Jack RH, West D, Adlam D, Peake M.
</span><span class="csl-right-inline">Impact on survival of modelling
increased surgical resection rates in patients with non-small-cell lung
cancer and cardiovascular comorbidities: A VICORI study. *British
Journal of Cancer* 2020;**123**:471–9.
<https://doi.org/10.1038/s41416-020-0869-8>.</span>

<span class="csl-left-margin">Booth S, Riley RD, Ensor J, Lambert PC,
Rutherford MJ. </span><span class="csl-right-inline">Temporal
recalibration for improving prognostic model development and risk
predictions in settings where survival is improving over time.
*International Journal of Epidemiology* 2020;**49**:1316–25.
<https://doi.org/10.1093/ije/dyaa030>.</span>

<span class="csl-left-margin">Syriopoulou E, Rutherford MJ, Lambert PC.
</span><span class="csl-right-inline">Marginal measures and causal
effects using the relative survival framework. *International Journal of
Epidemiology* 2020;**49**:619–28.
<https://doi.org/10.1093/ije/dyz268>.</span>

<span class="csl-left-margin">Arnold M, Rutherford MJ, Bardot A, Ferlay
J, Andersson TM, Myklebust TÅ, Tervonen H, Thursfield V, Ransom D, Shack
L, Woods RR, Turner D, Leonfellner S, Ryan S, Saint-Jacques N, De P,
McClure C, Ramanakumar AV, Stuart-Panko H, Engholm G, Walsh PM, Jackson
C, Vernon S, Morgan E, Gavin A, Morrison DS, Huws DW, Porter G, Butler
J, Bryant H, Currow DC, Hiom S, Parkin DM, Sasieni P, Lambert PC, Møller
B, Soerjomataram I, Bray F.
</span><span class="csl-right-inline">[<span class="nocase">Progress in
cancer survival, mortality, and incidence in seven high-income countries
1995-2014 (ICBP SURVMARK-2): a population-based
study</span>](https://www.ncbi.nlm.nih.gov/pubmed/31521509). *Lancet
Oncology* 2019;**20**:1493–505.</span>

<span class="csl-left-margin">Wong KF, Lambert PC, Mozumder SI, Broggio
J, Rutherford MJ. </span><span class="csl-right-inline">Conditional
crude probabilities of death for English cancer patients. *British
Journal of Cancer* 2019;**121**:883–9.
<https://doi.org/10.1038/s41416-019-0597-0>.</span>

<span class="csl-left-margin">Andersson TM-L, Rutherford MJ, Lambert PC.
</span><span class="csl-right-inline">Illustration of different
modelling assumptions for estimation of loss in expectation of life due
to cancer. *BMC Medical Research Methodology* 2019;**19**:19.
https://doi.org/<https://doi.org/10.1186/s12874-019-0785-x>.</span>

<span class="csl-left-margin">Bower H, Crowther MJ, Rutherford MJ,
Andersson TML, Clements M, Liu XR, Dickman PW, Lambert PC.
</span><span class="csl-right-inline">Capturing simple and complex
time-dependent effects using flexible parametric survival models:
*Communications in Statistics - Simulation and Computation* 2019.
https://doi.org/<https://doi.org/10.1080/03610918.2019.1634201>.</span>

<span class="csl-left-margin">Syriopoulou E, Morris E, P. F, Lambert PC,
Rutherford MJ. </span><span class="csl-right-inline">Understanding the
impact of socioeconomic differences in colorectal cancer survival:
Potential gain in life-years. *British Journal of Cancer*
2019;**120**:1052–8. <https://doi.org/10.1038/s41416-019-0455-0>.</span>

<span class="csl-left-margin">Rutherford MJ, Andersson TM-L, Björkholm
M, Lambert PC. </span><span class="csl-right-inline">Loss in life
expectancy and gain in life years as measures of cancer impact. *Cancer
Epidemiology* 2019;**60**:168–73.
<https://doi.org/10.1016/j.canep.2019.04.005>.</span>

<span class="csl-left-margin">Bower H, Andersson TM-L, Syriopoulou E,
Rutherford MJ, Lambe M, Ahlgren J, Dickman PW, Lambert PC.
</span><span class="csl-right-inline">Potential gain in life years for
Swedish women with breast cancer if stage and survival differences
between education groups could be eliminated - three what-if scenarios.
*Breast* 2019;**45**:75–81.
<https://doi.org/10.1016/j.breast.2019.03.005>.</span>

<span class="csl-left-margin">Weibull CE, Björkholm M, Glimelius I,
Lambert PC, Andersson TM-L, Smedby KE, Dickman PW, Eloranta S.
</span><span class="csl-right-inline">Temporal trends in
treatment-related incidence of diseases of the circulatory system among
Hodgkin lymphoma patients. *International Journal of Cancer*
2019;**145**:1200–8. <https://doi.org/10.1002/ijc.32142>.</span>

<span class="csl-left-margin">Syriopoulou E, Mozumder SI, Rutherford MJ,
Lambert PC. </span><span class="csl-right-inline">Robustness of
individual and marginal model-based estimates: A sensitivity analysis of
flexible parametric models. *Cancer Epidemiology* 2018;**58**:17–24.
<https://doi.org/10.1016/j.canep.2018.10.017>.</span>

<span class="csl-left-margin">Plym A, Bower H, Fredriksson I, Holmberg
L, Lambert PC, Lambe M. </span><span class="csl-right-inline">Loss in
working years after a breast cancer diagnosis. *British Journal of
Cancer* 2018;**118**:738–43.
<https://doi.org/10.1038/bjc.2017.456>.</span>

<span class="csl-left-margin">Mozumder SI, Dickman PW, Rutherford MJ,
Lambert PC. </span><span class="csl-right-inline">InterPreT cancer
survival: A dynamic web interactive prediction cancer survival tool for
health-care professionals and cancer epidemiologists. *Cancer
Epidemiology* 2018;**56**:46–52.
<https://doi.org/10.1016/j.canep.2018.07.009>.</span>

<span class="csl-left-margin">Weibull CE, Johansson ALV, Eloranta S,
Smedby KaE, Björkholm M, Lambert PC, Dickman PW, Glimelius I.
</span><span class="csl-right-inline">Contemporarily treated patients
with Hodgkin lymphoma have childbearing potential in line with matched
comparators. *Journal of Clinical Oncology* 2018;**36**:2718–25.
<https://doi.org/10.1200/JCO.2018.78.3514>.</span>

<span class="csl-left-margin">Latimer NR, Abrams KR, Lambert PC, Morden
JP, Crowther MJ. </span><span class="csl-right-inline">Assessing methods
for dealing with treatment switching in clinical trials: A follow-up
simulation study. *Statistical Methods in Medical Research*
2018;**27**:765–84. <https://doi.org/10.1177/0962280216642264>.</span>

<span class="csl-left-margin">Mozumder SI, Rutherford MJ, Lambert PC.
</span><span class="csl-right-inline">[stpm2cr: A flexible parametric
competing risks model using a direct likelihood approach for the
cause-specific cumulative incidence
function.](https://www.ncbi.nlm.nih.gov/pubmed/30305806) *The Stata
Journal* 2017;**17**:462–89.</span>

<span class="csl-left-margin">Mozumder SI, Lambert PC, Rutherford MJ.
</span><span class="csl-right-inline">Direct likelihood inference on the
cause-specific cumulative incidence function: A flexible parametric
regression modelling approach. *Statistics in Medicine*
2018;**37**:82–97. <https://doi.org/10.1002/sim.7498>.</span>

<span class="csl-left-margin">Bower H, Andersson TM-L, Crowther MJ,
Dickman PW, Lambe M, Lambert PC.
</span><span class="csl-right-inline">Adjusting expected mortality rates
using information from a control population: An example using
socioeconomic status. *American Journal of Epidemiology*
2018;**187**:828–36. <https://doi.org/10.1093/aje/kwx303>.</span>

<span class="csl-left-margin">Peters TL, Weibull CE, Fang F, Sandler DP,
Lambert PC, Ye W, Kamel F.
</span><span class="csl-right-inline">Association of fractures with the
incidence of amyotrophic lateral sclerosis. *Amyotrophic Lateral
Sclerosis & Frontotemporal Degeneration* 2017;**18**:419–25.
<https://doi.org/10.1080/21678421.2017.1300287>.</span>

<span class="csl-left-margin">Crowther MJ, Lambert PC.
</span><span class="csl-right-inline">Parametric multi-state survival
models: Flexible modelling allowing transition-specific distributions
with application to estimating clinically useful measures of effect
differences. *Statistics in Medicine* 2017;**36**:4719–42.
<https://doi.org/10.1002/sim.7448>.</span>

<span class="csl-left-margin">Cramb SM, Mengersen KL, Lambert PC, Ryan
LM, Baade PD. </span><span class="csl-right-inline">A flexible
parametric approach to examining spatial variation in relative survival.
*Statistics in Medicine* 2016;**35**:5448–63.
<https://doi.org/10.1002/sim.7071>.</span>

<span class="csl-left-margin">Lambert PC.
</span><span class="csl-right-inline">[The estimation and modelling of
cause-specific cumulative incidence functions using time-dependent
weights](http://www.stata-journal.com/article.html?article=st0471). *The
Stata Journal* 2017;**17**:181–207.</span>

<span class="csl-left-margin">Lambert PC, Wilkes SR, Crowther MJ.
</span><span class="csl-right-inline">Flexible parametric modelling of
the cause-specific cumulative incidence function. *Statistics in
Medicine* 2017;**36**:1429–46.
<https://doi.org/10.1002/sim.7208>.</span>

<span class="csl-left-margin">Syriopoulou E, Bower H, Andersson TM-L,
Lambert PC, Rutherford MJ.
</span><span class="csl-right-inline">Estimating the impact of a cancer
diagnosis on life expectancy by socio-economic group for a range of
cancer types in England. *British Journal of Cancer*
2017;**117**:1419–26. <https://doi.org/10.1038/bjc.2017.300>.</span>

<span class="csl-left-margin">Bower H, Andersson TML, Bjorkholm M,
Dickman PW, Lambert PC, Derolf AR.
</span><span class="csl-right-inline">Continued improvement in survival
of acute myeloid leukemia patients: An application of the loss in
expectation of life. *Blood Cancer Journal* 2016;**6**:e390.
<https://doi.org/10.1038/bcj.2016.3>.</span>

<span class="csl-left-margin">Bower H, Björkholm M, Dickman PW, Höglund
M, Lambert PC, Andersson TM-L.
</span><span class="csl-right-inline">Life expectancy of chronic myeloid
leukemia patients is approaching the life expectancy of the general
population. *Journal of Clinical Oncology* 2016;**34**:2851–7.
<https://doi.org/10.1200/JCO.2015.66.2866>.</span>

<span class="csl-left-margin">Edgren G, Hjalgrim H, Rostgaard K, Lambert
PC, Wikman A, Norda R, Titlestad K-E, Erikstrup C, Ullum H, Melbye M,
Busch MP, Nyrón O. </span><span class="csl-right-inline">Transmission of
neurodegenerative disorders through blood transfusion: A cohort study.
*Annals of Internal Medicine* 2016;**165**:316–24.
<https://doi.org/10.7326/M15-2421>.</span>

<span class="csl-left-margin">Crowther MJ, Andersson TM-L, Lambert PC,
Abrams KR, Humphreys K. </span><span class="csl-right-inline">Joint
modelling of longitudinal and survival data: Incorporating delayed entry
and an assessment of model misspecification. *Statistics in Medicine*
2016;**35**:1193–209. <https://doi.org/10.1002/sim.6779>.</span>

<span class="csl-left-margin">Andersson TM-L, Dickman PW, Eloranta S,
Sjövall A, Lambe M, Lambert PC.
</span><span class="csl-right-inline">The loss in expectation of life
after colon cancer: A population-based study. *BMC Cancer*
2015;**15**:412. <https://doi.org/10.1186/s12885-015-1427-2>.</span>

<span class="csl-left-margin">Hultcrantz M, Wilkes SR, Kristensson SY,
Andersson TM-L, Derolf A, Eloranta S, O. OL, Lambert PC, Björkholm M.
</span><span class="csl-right-inline">Risk and cause of death in 9,285
patients diagnosed with myeloproliferative neoplasms in Sweden between
1973 and 2005. A population-based study. *Journal of Clinical Oncology*
2015;**33**:2288–95. <https://doi.org/10.1200/JCO.2014.57.6652>.</span>

<span class="csl-left-margin">Rutherford MJ, Ironmonger L,
Ormiston-Smith N, Abel GA, Greenberg DC, Lyratzopoulos G, Lambert PC.
</span><span class="csl-right-inline">Estimating the potential survival
gains by eliminating socioeconomic and sex inequalities in stage at
diagnosis of melanoma. *British Journal of Cancer* 2015;**112
Suppl**:S116–23. <https://doi.org/10.1038/bjc.2015.50>.</span>

<span class="csl-left-margin">Crowther MJ, Lambert PC.
</span><span class="csl-right-inline">Reply to letter to the Editor by
Remontet et al. *Statistics in Medicine* 2015;**34**:3378–80.
<https://doi.org/10.1002/sim.6606>.</span>

<span class="csl-left-margin">Lambert PC, Dickman PW, Rutherford MJ.
</span><span class="csl-right-inline">Comparison of approaches to
estimating age-standardized net survival. *BMC Med Res Methodol*
2015;**15**:64. <https://doi.org/10.1186/s12874-015-0057-3>.</span>

<span class="csl-left-margin">Rutherford MJ, Andersson TM-L, Møller H,
Lambert PC. </span><span class="csl-right-inline">Understanding the
impact of socioeconomic differences in breast cancer survival in England
and Wales: Avoidable deaths and potential gain in expectation of life.
*Cancer Epidemiology* 2015;**39**:118–25.
<https://doi.org/10.1016/j.canep.2014.11.002>.</span>

<span class="csl-left-margin">Rutherford MJ, Crowther MJ, Lambert PC.
</span><span class="csl-right-inline">The use of restricted cubic
splines to approximate complex hazard functions in the analysis of
time-to-event data: A simulation study. *Journal of Statistical
Computation and Simulation* 2015;**85**:777–93.
<https://doi.org/10.1080/00949655.2013.845890>.</span>

<span class="csl-left-margin">Crowther MJ, Lambert PC.
</span><span class="csl-right-inline">A general framework for parametric
survival analysis. *Statistics in Medicine* 2014;**33**:5280–97.
<https://doi.org/10.1002/sim.6300>.</span>

<span class="csl-left-margin">Eloranta S, Lambert PC, Andersson TM-L,
Björkholm M, Dickman PW. </span><span class="csl-right-inline">The
application of cure models in the presence of competing risks: A tool
for improved risk communication in population-based cancer patient
survival. *Epidemiology* 2014;**25**:742–8.
<https://doi.org/10.1097/EDE.0000000000000130>.</span>

<span class="csl-left-margin">Feldman AL, Johansson ALV, Lambert PC,
Sieurin J, Yang F, Pedersen NL, Wirdefeldt K.
</span><span class="csl-right-inline">Familial coaggregation of
alzheimer’s disease and parkinson’s disease: Systematic review and
meta-analysis. *Neuroepidemiology* 2014;**42**:69–80.
<https://doi.org/10.1159/000355452>.</span>

<span class="csl-left-margin">Gao H, Hägg S, Sjögren P, Lambert PC,
Ingelsson E, Dam RM van. </span><span class="csl-right-inline">Serum
selenium in relation to measures of glucose metabolism and incidence of
type 2 diabetes in an older swedish population. *Diabetic Medicine : A
Journal of the British Diabetic Association* 2014;**31**:787–93.
<https://doi.org/10.1111/dme.12429>.</span>

<span class="csl-left-margin">Latimer NR, Abrams KR, Lambert PC,
Crowther MJ, Wailoo AJ, Morden JP, Akehurst RL, Campbell MJ.
</span><span class="csl-right-inline">Adjusting survival time estimates
to account for treatment switching in randomized controlled trials–an
economic evaluation context: Methods, limitations, and recommendations.
*Medical Decision Making* 2014;**34**:387–402.
<https://doi.org/10.1177/0272989X13520192>.</span>

<span class="csl-left-margin">Andersson TM-L, Eriksson H, Hansson J,
Månsson-Brahme E, Dickman PW, Eloranta S, Lambe M, Lambert PC.
</span><span class="csl-right-inline">Estimating the cure proportion of
malignant melanoma, an alternative approach to assess long term
survival: A population-based study. *Cancer Epidemiology*
2014;**38**:93–9. <https://doi.org/10.1016/j.canep.2013.12.006>.</span>

<span class="csl-left-margin">Andersson TM-L, Dickman PW, Eloranta S,
Lambe M, Lambert PC. </span><span class="csl-right-inline">Estimating
the loss in expectation of life due to cancer using flexible parametric
survival models. *Statistics in Medicine* 2013;**32**:5286–300.
<https://doi.org/10.1002/sim.5943>.</span>

<span class="csl-left-margin">Ahyow LC, Lambert PC, Jenkins DR, Neal KR,
Tobin M. </span><span class="csl-right-inline">Bed occupancy rates and
hospital-acquired clostridium difficile infection: A cohort study.
*Infection Control and Hospital Epidemiology* 2013;**34**:1062–9.
<https://doi.org/10.1086/673156>.</span>

<span class="csl-left-margin">Crowther MJ, Abrams KR, Lambert PC.
</span><span class="csl-right-inline">Joint modelling of longitudinal
and survival data. *The Stata Journal* 2013;**13**:165–84.</span>

<span class="csl-left-margin">Crowther MJ, Lambert PC.
</span><span class="csl-right-inline">Stgenreg: A stata package for
general parametric survival analysis. *Journal of Statistical Software*
2013;**53**:1–17. <https://doi.org/10.18637/jss.v053.i12>.</span>

<span class="csl-left-margin">Crowther MJ, Lambert PC.
</span><span class="csl-right-inline">Simulating biologically plausible
complex survival data. *Statistics in Medicine* 2013;**32**:4118–34.
<https://doi.org/10.1002/sim.5823>.</span>

<span class="csl-left-margin">Crowther MJ, Lambert PC, Abrams KR.
</span><span class="csl-right-inline">Adjusting for measurement error in
baseline prognostic biomarkers included in a time-to-event analysis: A
joint modelling approach. *BMC Medical Research Methodology*
2013;**13**:146. <https://doi.org/10.1186/1471-2288-13-146>.</span>

<span class="csl-left-margin">Cvancarova M, Aagnes B, Fosså SD, Lambert
PC, Möller B., Bray F. </span><span class="csl-right-inline">Proportion
cured models applied to 23 cancer sites in Norway. *International
Journal of Cancer* 2013;**132**:1700–10.
<https://doi.org/10.1002/ijc.27802>.</span>

<span class="csl-left-margin">Eloranta S, Adolfsson J, Lambert PC,
Stattin P, Akre O, Andersson TM-L, Dickman PW.
</span><span class="csl-right-inline">How can we make cancer survival
statistics more useful for patients and clinicians: An illustration
using localized prostate cancer in Sweden. *Cancer Causes Control*
2013;**24**:505–15. <https://doi.org/10.1007/s10552-012-0141-5>.</span>

<span class="csl-left-margin">Eloranta S, Lambert PC, Sjöberg J,
Andersson TM-L, Björkholm M, Dickman PW.
</span><span class="csl-right-inline">Temporal trends in mortality from
diseases of the circulatory system after treatment for Hodgkin lymphoma:
A population-based cohort study in Sweden (1973 to 2006). *Journal of
Clinical Oncology* 2013;**31**:1435–41.
<https://doi.org/10.1200/JCO.2012.45.2714>.</span>

<span class="csl-left-margin">Hinchliffe SR, Abrams KR, Lambert PC.
</span><span class="csl-right-inline">The impact of under and
over-recording of cancer on death certificates in a competing risks
analysis: A simulation study. *Cancer Epidemiology* 2013;**37**:11–9.
<https://doi.org/10.1016/j.canep.2012.08.012>.</span>

<span class="csl-left-margin">Hinchliffe SR, Lambert PC.
</span><span class="csl-right-inline">Flexible parametric modelling of
cause-specific hazards to estimate cumulative incidence functions. *BMC
Medical Research Methodology* 2013;**13**:13.
<https://doi.org/10.1186/1471-2288-13-13>.</span>

<span class="csl-left-margin">Hinchliffe SR, Lambert PC.
</span><span class="csl-right-inline">Extending the flexible parametric
survival model for competing risks. *The Stata Journal*
2013;**13**:344–55.</span>

<span class="csl-left-margin">Hinchliffe SR, Scott DA, Lambert PC.
</span><span class="csl-right-inline">Flexible parametric illness-death
models. *The Stata Journal* 2013;**13**:759–75.</span>

<span class="csl-left-margin">Hinchliffe SR, Seaton SE, Lambert PC,
Draper ES, Field DJ, Manktelow BN.
</span><span class="csl-right-inline">Modelling time to death or
discharge in neonatal care: An application of competing risks. *Paediatr
Perinat Epidemiol* 2013;**27**:426–33.
<https://doi.org/10.1111/ppe.12053>.</span>

<span class="csl-left-margin">Rutherford MJ, Hinchliffe SR, Abel GA,
Lyratzopoulos G, Lambert PC, Greenberg DC.
</span><span class="csl-right-inline">How much of the deprivation gap in
cancer survival can be explained by variation in stage at diagnosis: An
example from breast cancer in the East of England. *International
Journal of Cancer* 2013;**133**:2192–200.
<https://doi.org/10.1002/ijc.28221>.</span>

<span class="csl-left-margin">Rutherford MJ, Møller H, Lambert PC.
</span><span class="csl-right-inline">A comprehensive assessment of the
impact of errors in the cancer registration process on 1- and 5-year
relative survival estimates. *British Journal of Cancer*
2013;**108**:691–8. <https://doi.org/10.1038/bjc.2013.12>.</span>

<span class="csl-left-margin">Dickman PW, Lambert PC, Coviello E,
Rutherford MJ. </span><span class="csl-right-inline">Estimating net
survival in population-based cancer studies. *International Journal of
Cancer* 2013;**133**:519–21. <https://doi.org/10.1002/ijc.28041>.</span>

<span class="csl-left-margin">Shah A, Andersson TM-L, Rachet B,
Björkholm M, Lambert PC. </span><span class="csl-right-inline">Survival
and cure of acute myeloid leukaemia in England, 1971-2006: A
population-based study. *British Journal of Haematology*
2013;**162**:509–16. <https://doi.org/10.1111/bjh.12425>.</span>

<span class="csl-left-margin">Yu XQ, De Angelis R, Andersson TML,
Lambert PC, O’Connell DL, Dickman PW.
</span><span class="csl-right-inline">Estimating the proportion cured of
cancer: Some practical advice for users. *Cancer Epidemiology*
2013;**37**:836–42.
<https://doi.org/10.1016/j.canep.2013.08.014>.</span>

<span class="csl-left-margin">Andersson TM-L, Lambert PC.
</span><span class="csl-right-inline">Fitting and modeling cure in
population-based cancer studies within the framework of flexible
parametric survival models. *The Stata Journal*
2012;**12**:623–8.</span>

<span class="csl-left-margin">Andrae B, Andersson TM-L, Lambert PC,
Kemetli L, Silfverdal L, Strander B, Ryd W, Dillner J, Törnberg S,
Sparén P. </span><span class="csl-right-inline">Screening and cervical
cancer cure: Population based cohort study. *BMJ* 2012;**344**:e900.
<https://doi.org/10.1136/bmj.e900>.</span>

<span class="csl-left-margin">Crowther MJ, Abrams KR, Lambert PC.
</span><span class="csl-right-inline">Flexible parametric joint
modelling of longitudinal and survival data. *Statistics in Medicine*
2012;**31**:4456–71. <https://doi.org/10.1002/sim.5644>.</span>

<span class="csl-left-margin">Crowther MJ, Lambert PC.
</span><span class="csl-right-inline">Simulating complex survival data.
*The Stata Journal* 2012;**12**:674–87.</span>

<span class="csl-left-margin">Crowther MJ, Riley RD, Staessen JA, Wang
J, Gueyffier F, Lambert PC.
</span><span class="csl-right-inline">Individual patient data
meta-analysis of survival data using Poisson regression models. *BMC
Medical Research Methodology* 2012;**12**:34.
<https://doi.org/10.1186/1471-2288-12-34>.</span>

<span class="csl-left-margin">Eloranta S, Lambert PC, Andersson TM-L,
Czene K, Hall P, Björkholm M, Dickman PW.
</span><span class="csl-right-inline">Partitioning of excess mortality
in population-based cancer patient survival studies using flexible
parametric survival models. *BMC Medical Research Methodology*
2012;**12**:86. <https://doi.org/10.1186/1471-2288-12-86>.</span>

<span class="csl-left-margin">Hinchliffe SR, Dickman PW, Lambert PC.
</span><span class="csl-right-inline">Adjusting for the proportion of
cancer deaths in the general population when using relative survival: A
sensitivity analysis. *Cancer Epidemiology* 2012;**36**:148–52.
<https://doi.org/10.1016/j.canep.2011.09.007>.</span>

<span class="csl-left-margin">Hinchliffe SR, Rutherford MJ, Crowther MJ,
Nelson CP, Lambert PC. </span><span class="csl-right-inline">Should
relative survival be used with lung cancer data? *British Journal of
Cancer* 2012;**106**:1854–9.
<https://doi.org/10.1038/bjc.2012.182>.</span>

<span class="csl-left-margin">Holmberg L, Robinson D, Sandin F, Bray F,
Linklater KM, Klint A, Lambert PC, Adolfsson J, Hamdy FC, Catto J,
Møller H. </span><span class="csl-right-inline">A comparison of prostate
cancer survival in England, Norway and Sweden: A population-based study.
*Cancer Epidemiology* 2012;**36**:e7–12.
<https://doi.org/10.1016/j.canep.2011.08.001>.</span>

<span class="csl-left-margin">Møller H, Sandin F, Robinson D, Bray F,
Klint S, Linklater KM, Lambert PC, Påhlman L, Holmberg L, Morris E.
</span><span class="csl-right-inline">Colorectal cancer survival in
socioeconomic groups in England: Variation is mainly in the short term
after diagnosis. *European Journal of Cancer* 2012;**48**:46–53.
<https://doi.org/10.1016/j.ejca.2011.05.018>.</span>

<span class="csl-left-margin">Rutherford MJ, Dickman PW, Lambert PC.
</span><span class="csl-right-inline">Comparison of methods for
calculating relative survival in population-based studies. *Cancer
Epidemiology* 2012;**36**:16–21.
<https://doi.org/10.1016/j.canep.2011.05.010>.</span>

<span class="csl-left-margin">Rutherford MJ, Thompson JR, Lambert PC.
</span><span class="csl-right-inline">Projecting cancer incidence using
age-period-cohort models incorporating restricted cubic splines.
*International Journal of Biostatistics* 2012;**8**:33.
<https://doi.org/10.1515/1557-4679.1411>.</span>

<span class="csl-left-margin">Shack LG, Shah A, Lambert PC, Rachet B.
</span><span class="csl-right-inline">Cure by age and stage at diagnosis
for colorectal cancer patients in North West England, 1997-2004: A
population-based study. *Cancer Epidemiology* 2012;**36**:548–53.
<https://doi.org/10.1016/j.canep.2012.06.011>.</span>

<span class="csl-left-margin">Andersson TM-L, Dickman PW, Eloranta S,
Lambert PC. </span><span class="csl-right-inline">Estimating and
modelling cure in population-based cancer studies within the framework
of flexible parametric survival models. *BMC Medical Research
Methodology* 2011;**11**:96.
<https://doi.org/10.1186/1471-2288-11-96>.</span>

<span class="csl-left-margin">Eaker S, Wigertz A, Lambert PC, Bergkvist
L, Ahlgren J, Lambe M, Uppsala/Örebro Breast Cancer Group.
</span><span class="csl-right-inline">Breast cancer, sickness absence,
income and marital status. A study on life situation 1 year prior
diagnosis compared to 3 and 5 years after diagnosis. *PLoS One*
2011;**6**:e18040.
<https://doi.org/10.1371/journal.pone.0018040>.</span>

<span class="csl-left-margin">Hakulinen T, Seppä K, Lambert PC.
</span><span class="csl-right-inline">Choosing the relative survival
method for cancer survival estimation. *European Journal of Cancer*
2011;**47**:2202–10.
<https://doi.org/10.1016/j.ejca.2011.03.011>.</span>

<span class="csl-left-margin">Lambert PC, Holmberg L, Sandin F, Bray F,
Linklater KM, Purushotham A, Robinson D, Møller H.
</span><span class="csl-right-inline">Quantifying differences in breast
cancer survival between England and Norway. *Cancer Epidemiology*
2011;**35**:526–33.
<https://doi.org/10.1016/j.canep.2011.04.003>.</span>

<span class="csl-left-margin">Coleman MP, Rachet B, Woods L, Berrino F,
Butler J, Capocaccia R, Dickman PW, Gavin A, Giorgi R, Hamilton W,
Lambert PC, Peake MD, Perme MP, Stare J, Vedstedt P.
</span><span class="csl-right-inline">Rebuttal to editorial saying
cancer survival statistics are misleading. *BMJ* 2011;**343**:d4214.
<https://doi.org/10.1136/bmj.d4214>.</span>

<span class="csl-left-margin">Morden JP, Lambert PC, Latimer N, Abrams
KR, Wailoo AJ. </span><span class="csl-right-inline">Assessing methods
for dealing with treatment switching in randomised controlled trials: A
simulation study. *BMC Medical Research Methodology* 2011;**11**:572–82.
<https://doi.org/10.1093/biostatistics/kxq007>.</span>

<span class="csl-left-margin">Morris EJA, Sandin F, Lambert PC, Bray F,
Klint Å, Linklater K, Robinson D, Påhlman L, Holmberg L, Møller H.
</span><span class="csl-right-inline">A population-based comparison of
the survival of patients with colorectal cancer in England, Norway and
Sweden between 1996 and 2004. *Gut* 2011;**60**:1087–93.
<https://doi.org/10.1136/gut.2010.229575>.</span>

<span class="csl-left-margin">Andersson TM-L, Lambert PC, Derolf AR,
Kristinsson SY, Eloranta S, Landgren O, Björkholm M, Dickman PW.
</span><span class="csl-right-inline">Temporal trends in the proportion
cured among adults diagnosed with acute myeloid leukaemia in Sweden
1973-2001, a population-based study. *Br J Haematol*
2010;**148**:918–24.
<https://doi.org/10.1111/j.1365-2141.2009.08026.x>.</span>

<span class="csl-left-margin">Eloranta S, Lambert PC, Cavalli-Björkman
N, Andersson TM-L, Glimelius B, Dickman PW.
</span><span class="csl-right-inline">Does socioeconomic status
influence the prospect of cure from colon cancer–a population-based
study in Sweden 1965-2000. *European Journal of Cancer*
2010;**46**:2965–72.
<https://doi.org/10.1016/j.ejca.2010.05.028>.</span>

<span class="csl-left-margin">Lambert PC, Dickman PW, Nelson CP, Royston
P. </span><span class="csl-right-inline">Estimating the crude
probability of death due to cancer and other causes using relative
survival models. *Statistics in Medicine* 2010;**29**:885–95.
<https://doi.org/10.1002/sim.3762>.</span>

<span class="csl-left-margin">Lambert PC, Dickman PW, Weston CL,
Thompson JR. </span><span class="csl-right-inline">Estimating the cure
fraction in population-based cancer studies by using finite mixture
models. *Journal of the Royal Statistical Society, Series C*
2010;**59**:35–55.
<https://doi.org/10.1111/j.1467-9876.2009.00677.x>.</span>

<span class="csl-left-margin">Riley RD, Lambert PC, Abo-Zaid G.
</span><span class="csl-right-inline">Meta-analysis of individual
participant data: Rationale, conduct, and reporting. *BMJ*
2010;**340**:c221. <https://doi.org/10.1136/bmj.c221>.</span>

<span class="csl-left-margin">Rutherford MJ, Lambert PC, Thompson JR.
</span><span class="csl-right-inline">Age-period-cohort modelling. *The
Stata Journal* 2010;**10**:606–27.
<https://doi.org/10.1038/bjc.2015.51>.</span>

<span class="csl-left-margin">Squire IB, Nelson CP, Ng LL, Jones DR,
Woods KL, Lambert PC. </span><span class="csl-right-inline">Prognostic
value of admission blood glucose concentration and diabetes diagnosis on
survival after acute myocardial infarction; results from 4702 index
cases in routine practice. *Clinical Science* 2010;**118**:527–35.
<https://doi.org/10.1042/CS20090322>.</span>

<span class="csl-left-margin">Lambert PC, Royston P.
</span><span class="csl-right-inline">Further development of flexible
parametric models for survival analysis. *The Stata Journal*
2009;**9**:265–90.</span>

<span class="csl-left-margin">Larfors G, Lambert PC, Lambe M, Ekbom A,
Cnattingius S. </span><span class="csl-right-inline">Placental weight
and breast cancer survival in young women. *Cancer Epidemiol Biomarkers
Prev* 2009;**18**:777–83.
<https://doi.org/10.1158/1055-9965.EPI-08-0979>.</span>

<span class="csl-left-margin">Panickar J, Lakhanpaul M, Lambert PC,
Kenia P, Stephenson T, Smyth A, Grigg J.
</span><span class="csl-right-inline">Oral prednisolone for preschool
children with acute virus-induced wheezing. *New England Journal of
Medicine* 2009;**360**:329–38.
<https://doi.org/10.1056/NEJMoa0804897>.</span>

<span class="csl-left-margin">Woods LM, Rachet B, Lambert PC, Coleman
MP. </span><span class="csl-right-inline">‘Cure’ from breast cancer
among two populations of women followed for 23 years after diagnosis.
*Annals of Oncology* 2009;**20**:1331–6.
<https://doi.org/10.1093/annonc/mdn791>.</span>

<span class="csl-left-margin">Bhaskaran K, Hamouda O, Sannes M, Boufassa
F, Johnson AM, Lambert PC, Porter K, CASCADE Collaboration.
</span><span class="csl-right-inline">Changes in the risk of death after
HIV seroconversion compared with mortality in the general population.
*JAMA* 2008;**300**:51–9.
<https://doi.org/10.1001/jama.300.1.51>.</span>

<span class="csl-left-margin">Gillies CL, Lambert PC, Abrams KR, Sutton
AJ, Cooper NJ, Hsu RT, Davies MJ, Khunti K.
</span><span class="csl-right-inline">Different strategies for screening
and prevention of type 2 diabetes in adults: Cost effectiveness
analysis. *BMJ* 2008;**336**:1180–5.
<https://doi.org/10.1136/bmj.39545.585289.25>.</span>

<span class="csl-left-margin">Lambert PC, Billingham LJ, Cooper NJ,
Sutton AJ, Abrams KR. </span><span class="csl-right-inline">Estimating
the cost-effectiveness of an intervention in a clinical trial when
partial cost information is available: A Bayesian approach. *Health
Economics* 2008;**17**:67–81. <https://doi.org/10.1002/hec.1243>.</span>

<span class="csl-left-margin">Nelson CP, Lambert PC, Squire IB, Jones
DR. </span><span class="csl-right-inline">Relative survival: What can
cardiovascular disease learn from cancer? *European Heart Journal*
2008;**29**:941–7. <https://doi.org/10.1093/eurheartj/ehn079>.</span>

<span class="csl-left-margin">Lambert PC, Sutton AJ, Burton PR, Abrams
KR, Jones DR. </span><span class="csl-right-inline">Comments on ’trying
to be precise about vagueness’ by stephen senn, statistics in medicine
2007; 26:1417-1430. *Statistics in Medicine* 2008;**27**:619–22, author
reply 622–4. <https://doi.org/10.1002/sim.3043>.</span>

<span class="csl-left-margin">Reynolds R, Lambert PC, Burton PR, B. S.
A. C. Extended Working Parties on Resistance Surveillance.
</span><span class="csl-right-inline">Analysis, power and design of
antimicrobial resistance surveillance studies, taking account of
inter-centre variation and turnover. *J Antimicrob Chemother* 2008;**62
Suppl 2**:ii29–39. <https://doi.org/10.1093/jac/dkn350>.</span>

<span class="csl-left-margin">Riley RD, Lambert PC, Staessen JA, Wang J,
Gueyffier F, Thijs L, Boutitie F.
</span><span class="csl-right-inline">Meta-analysis of continuous
outcomes combining individual patient data and aggregate data.
*Statistics in Medicine* 2008;**27**:1870–93.
<https://doi.org/10.1002/sim.3165>.</span>

<span class="csl-left-margin">Cooper NJ, Lambert PC, Abrams KR, Sutton
AJ. </span><span class="csl-right-inline">Predicting costs over time
using Bayesian Markov chain Monte Carlo methods: An application to early
inflammatory polyarthritis. *Health Economics* 2007;**16**:37–56.
<https://doi.org/10.1002/hec.1141>.</span>

<span class="csl-left-margin">Gillies CL, Abrams KR, Lambert PC, Cooper
NJ, Sutton AJ, Hsu RT, Khunti K.
</span><span class="csl-right-inline">Pharmacological and lifestyle
interventions to prevent or delay type 2 diabetes in people with
impaired glucose tolerance: Systematic review and meta-analysis. *BMJ*
2007;**334**:299. <https://doi.org/10.1136/bmj.39063.689375.55>.</span>

<span class="csl-left-margin">Lambert PC.
</span><span class="csl-right-inline">Modeling of the cure fraction in
survival studies. *The Stata Journal* 2007;**7**:351–75.</span>

<span class="csl-left-margin">Lambert PC, Dickman PW, Österlund P,
Andersson TM-L, Sankila R, Glimelius B.
</span><span class="csl-right-inline">Temporal trends in the proportion
cured for cancer of the colon and rectum: A population-based study using
data from the Finnish cancer registry. *International Journal of Cancer*
2007;**121**:2052–9. <https://doi.org/10.1002/ijc.22948>.</span>

<span class="csl-left-margin">Lambert PC, Thompson JR, Weston CL,
Dickman PW. </span><span class="csl-right-inline">Estimating and
modeling the cure fraction in population-based cancer survival analysis.
*Biostatistics* 2007;**8**:576–94.
<https://doi.org/10.1093/biostatistics/kxl030>.</span>

<span class="csl-left-margin">Manca A, Lambert PC, Sculpher M, Rice N.
</span><span class="csl-right-inline">Cost-effectiveness analysis using
data from multinational trials: The use of bivariate hierarchical
modeling. *Medical Decision Making* 2007;**27**:471–90.
<https://doi.org/10.1177/0272989X07302132>.</span>

<span class="csl-left-margin">Nelson CP, Lambert PC, Squire IB, Jones
DR. </span><span class="csl-right-inline">Flexible parametric models for
relative survival, with application in coronary heart disease.
*Statistics in Medicine* 2007;**26**:5486–98.
<https://doi.org/10.1002/sim.3064>.</span>

<span class="csl-left-margin">Riley RD, Abrams KR, Lambert PC, Sutton
AJ, Thompson JR. </span><span class="csl-right-inline">An evaluation of
bivariate random-effects meta-analysis for the joint synthesis of two
correlated outcomes. *Statistics in Medicine* 2007;**26**:78–97.
<https://doi.org/10.1002/sim.2524>.</span>

<span class="csl-left-margin">Riley RD, Abrams KR, Sutton AJ, Lambert
PC, Thompson JR. </span><span class="csl-right-inline">Bivariate
random-effects meta-analysis and the estimation of between-study
correlation. *BMC Medical Research Methodology* 2007;**7**:3.
<https://doi.org/10.1186/1471-2288-7-3>.</span>

<span class="csl-left-margin">Sutton AJ, Cooper NJ, Jones DR, Lambert
PC, Thompson JR, Abrams KR.
</span><span class="csl-right-inline">Evidence-based sample size
calculations based upon updated meta-analysis. *Statistics in Medicine*
2007;**26**:2479–500. <https://doi.org/10.1002/sim.2704>.</span>

<span class="csl-left-margin">Abrams KR, Gillies CL, Lambert PC.
</span><span class="csl-right-inline">Meta-analysis of heterogeneously
reported trials assessing change from baseline. *Statistics in Medicine*
2005;**24**:3823–44. <https://doi.org/10.1002/sim.2423>.</span>

<span class="csl-left-margin">Lambert PC, Smith LK, Jones DR, Botha JL.
</span><span class="csl-right-inline">Additive and multiplicative
covariate regression models for relative survival incorporating
fractional polynomials for time-dependent effects. *Statistics in
Medicine* 2005;**24**:3871–85.
<https://doi.org/10.1002/sim.2399>.</span>

<span class="csl-left-margin">Lambert PC, Sutton AJ, Burton PR, Abrams
KR, Jones DR. </span><span class="csl-right-inline">How vague is vague?
A simulation study of the impact of the use of vague prior distributions
in MCMC using WinBUGS. *Statistics in Medicine* 2005;**24**:2401–28.
<https://doi.org/10.1002/sim.2112>.</span>

<span class="csl-left-margin">Minelli C, Thompson JR, Abrams KR, Lambert
PC. </span><span class="csl-right-inline">Bayesian implementation of a
genetic model-free approach to the meta-analysis of genetic association
studies. *Statistics in Medicine* 2005;**24**:3845–61.
<https://doi.org/10.1002/sim.2393>.</span>

<span class="csl-left-margin">Sutton AJ, Cooper NJ, Abrams KR, Lambert
PC, Jones DR. </span><span class="csl-right-inline">A Bayesian approach
to evaluating net clinical benefit allowed for parameter uncertainty.
*Journal of Clinical Epidemiology* 2005;**58**:26–40.
<https://doi.org/10.1016/j.jclinepi.2004.03.015>.</span>

<span class="csl-left-margin">Taub NA, Morgan Z, Brugha TS, Lambert PC,
Bebbington PE, Jenkins R, Kessler RC, Zaslavsky AM, Hotz T.
</span><span class="csl-right-inline">[Recalibration methods to enhance
information on prevalence rates from large mental health
surveys.](https://www.ncbi.nlm.nih.gov/pubmed/16097396) *Int J Methods
Psychiatr Res* 2005;**14**:3–13.</span>

<span class="csl-left-margin">Lambert PC, Burton PR, Abrams KR, Brooke
AM. </span><span class="csl-right-inline">The analysis of peak
expiratory flow data using a three-level hierarchical model. *Statistics
in Medicine* 2004;**23**:3821–39.
<https://doi.org/10.1002/sim.1951>.</span>

<span class="csl-left-margin">Riley RD, Heney D, Jones DR, Sutton AJ,
Lambert PC, Abrams KR, Young B, Wailoo AJ, Burchill SA.
</span><span class="csl-right-inline">[A systematic review of molecular
and biological tumor markers in
neuroblastoma.](https://www.ncbi.nlm.nih.gov/pubmed/14734444) *Clinical
Cancer Research* 2004;**10**:4–12.</span>

<span class="csl-left-margin">Riley RD, Sutton AJ, Abrams KR, Lambert
PC. </span><span class="csl-right-inline">Sensitivity analyses allowed
more appropriate and reliable meta-analysis conclusions for multiple
outcomes when missing data was present. *Journal of Clinical
Epidemiology* 2004;**57**:911–24.
<https://doi.org/10.1016/j.jclinepi.2004.01.018>.</span>

<span class="csl-left-margin">Smith LK, Lambert PC, Botha JL, Jones DR.
</span><span class="csl-right-inline">Providing more up-to-date
estimates of patient survival: A comparison of standard survival
analysis with period analysis using life-table methods and proportional
hazards models. *Journal of Clinical Epidemiology* 2004;**57**:14–20.
<https://doi.org/10.1016/S0895-4356(03)00253-1>.</span>

<span class="csl-left-margin">Sweeting MJ, Sutton AJ, Lambert PC.
</span><span class="csl-right-inline">What to add to nothing? Use and
avoidance of continuity corrections in meta-analysis of sparse data.
*Statistics in Medicine* 2004;**23**:1351–75.
<https://doi.org/10.1002/sim.1761>.</span>

<span class="csl-left-margin">Baker R, Smith JF, Lambert PC.
</span><span class="csl-right-inline">[Randomised controlled trial of
the effectiveness of feedback in improving test ordering in general
practice.](https://www.ncbi.nlm.nih.gov/pubmed/14695072) *Scand J Prim
Health Care* 2003;**21**:219–23.</span>

<span class="csl-left-margin">Hsu RT, Lambert PC, Dixon-Woods M,
Kurinczuk JJ. </span><span class="csl-right-inline">Effect of NHS
walk-in centre on local primary healthcare services: Before and after
observational study. *BMJ* 2003;**326**:530.
<https://doi.org/10.1136/bmj.326.7388.530>.</span>

<span class="csl-left-margin">Mckean MC, Hewitt C, Lambert PC, Myint S,
Silverman M. </span><span class="csl-right-inline">[An adult model of
exclusive viral wheeze: Inflammation in the upper and lower respiratory
tracts.](https://www.ncbi.nlm.nih.gov/pubmed/12859447) *Clin Exp
Allergy* 2003;**33**:912–20.</span>

<span class="csl-left-margin">Oommen A, Lambert PC, Grigg J.
</span><span class="csl-right-inline">Efficacy of a short course of
parent-initiated oral prednisolone for viral wheeze in children aged 1-5
years: Randomised controlled trial. *Lancet* 2003;**362**:1433–8.
<https://doi.org/10.1016/S0140-6736(03)14685-5>.</span>

<span class="csl-left-margin">Riley RD, Abrams KR, Sutton AJ, Lambert
PC, Jones DR, Heney D, Burchill SA.
</span><span class="csl-right-inline">Reporting of prognostic markers:
Current problems and development of guidelines for evidence-based
practice in the future. *British Journal of Cancer* 2003;**88**:1191–8.
<https://doi.org/10.1038/sj.bjc.6600886>.</span>

<span class="csl-left-margin">Riley RD, Burchill SA, Abrams KR, Heney D,
Lambert PC, Jones DR, Sutton AJ, Young B, Wailoo AJ, Lewis IJ.
</span><span class="csl-right-inline">[A systematic review and
evaluation of the use of tumour markers in paediatric oncology: Ewing’s
sarcoma and
neuroblastoma.](https://www.ncbi.nlm.nih.gov/pubmed/12633526) *Health
Technol Assess* 2003;**7**:1–162.</span>

<span class="csl-left-margin">Riley RD, Burchill SA, Abrams KR, Heney D,
Sutton AJ, Jones DR, Lambert PC, Young B, Wailoo AJ, Lewis IJ.
</span><span class="csl-right-inline">[A systematic review of molecular
and biological markers in tumours of the ewing’s sarcoma
family.](https://www.ncbi.nlm.nih.gov/pubmed/12504654) *European Journal
of Cancer* 2003;**39**:19–30.</span>

<span class="csl-left-margin">Smith LK, Lambert PC, Jones DR.
</span><span class="csl-right-inline">Up-to-date estimates of long-term
cancer survival in England and Wales. *British Journal of Cancer*
2003;**89**:74–6. <https://doi.org/10.1038/sj.bjc.6600976>.</span>

<span class="csl-left-margin">Waugh J, Bell SC, Kilby MD, Lambert PC,
Blackwell CN, Shennan A, Halligan A.
</span><span class="csl-right-inline">Urinary microalbumin/creatinine
ratios: Reference range in uncomplicated pregnancy. *Clinical Science*
2003;**104**:103–7. <https://doi.org/10.1042/CS20020170>.</span>

<span class="csl-left-margin">Lambert PC, Sutton AJ, Abrams KR, Jones
DR. </span><span class="csl-right-inline">[A comparison of summary
patient-level covariates in meta-regression with individual patient data
meta-analysis.](https://www.ncbi.nlm.nih.gov/pubmed/11781126) *Journal
of Clinical Epidemiology* 2002;**55**:86–94.</span>

<span class="csl-left-margin">Young B, Fitch GE, Dixon-Woods M, Lambert
PC, Brooke AM. </span><span class="csl-right-inline">[Parents’ accounts
of wheeze and asthma related symptoms: A qualitative
study.](https://www.ncbi.nlm.nih.gov/pubmed/12138062) *Arch Dis Child*
2002;**87**:131–4.</span>

<span class="csl-left-margin">Lambert PC, Abrams KR, Jones DR, Halligan
AW, Shennan A. </span><span class="csl-right-inline">[Analysis of
ambulatory blood pressure monitor data using a hierarchical model
incorporating restricted cubic splines and heterogeneous within-subject
variances.](https://www.ncbi.nlm.nih.gov/pubmed/11782034) *Statistics in
Medicine* 2001;**20**:3789–805.</span>

<span class="csl-left-margin">Mckean MC, Leech M, Lambert PC, Hewitt C,
Myint S, Silverman M. </span><span class="csl-right-inline">[A model of
viral wheeze in nonasthmatic adults: Symptoms and
physiology.](https://www.ncbi.nlm.nih.gov/pubmed/11510797) *Eur Respir
J* 2001;**18**:23–32.</span>

<span class="csl-left-margin">Waugh J, Perry IJ, Halligan AW, Swiet MD,
Lambert PC, Penny JA, Taylor DJ, Jones DR, Shennan A.
</span><span class="csl-right-inline">Birth weight and 24-hour
ambulatory blood pressure in nonproteinuric hypertensive pregnancy. *Am
J Obstet Gynecol* 2000;**183**:633–7.
<https://doi.org/10.1067/mob.2000.106448>.</span>

<span class="csl-left-margin">Bell SC, Halligan AW, Martin A, Ashmore J,
Shennan AH, Lambert PC, Taylor DJ.
</span><span class="csl-right-inline">[The role of observer error in
antenatal dipstick proteinuria
analysis.](https://www.ncbi.nlm.nih.gov/pubmed/10549963) *Br J Obstet
Gynaecol* 1999;**106**:1177–80.</span>

<span class="csl-left-margin">Williams N, Jackson D, Lambert PC,
Johnstone JM. </span><span class="csl-right-inline">[Incidence of
non-specific abdominal pain in children during school term: Population
survey based on discharge
diagnoses.](https://www.ncbi.nlm.nih.gov/pubmed/10346771) *BMJ*
1999;**318**:1455.</span>

<span class="csl-left-margin">Brooke AM, Lambert PC, Burton PR, Clarke
C, Luyt DK, Simpson H. </span><span class="csl-right-inline">[Recurrent
cough: Natural history and significance in infancy and early
childhood.](https://www.ncbi.nlm.nih.gov/pubmed/9811075) *Pediatr
Pulmonol* 1998;**26**:256–61.</span>

<span class="csl-left-margin">Hellmich M, Abrams KR, Jones DR, Lambert
PC. </span><span class="csl-right-inline">[A Bayesian approach to a
general regression model for ROC
curves.](https://www.ncbi.nlm.nih.gov/pubmed/10372587) *Medical Decision
Making* 1998;**18**:436–43.</span>

<span class="csl-left-margin">Penny JA, Halligan AW, Shennan AH, Lambert
PC, Jones DR, Swiet M de, Taylor DJ.
</span><span class="csl-right-inline">[Automated, ambulatory, or
conventional blood pressure measurement in pregnancy: Which is the
better predictor of severe
hypertension?](https://www.ncbi.nlm.nih.gov/pubmed/9539520) *Am J Obstet
Gynecol* 1998;**178**:521–6.</span>

<span class="csl-left-margin">Halligan AW, Shennan A, Lambert PC, Bell
SC, Taylor DJ, Swiet M de.
</span><span class="csl-right-inline">[Automated blood pressure
measurement as a predictor of proteinuric
pre-eclampsia.](https://www.ncbi.nlm.nih.gov/pubmed/9166197) *Br J
Obstet Gynaecol* 1997;**104**:559–62.</span>

<span class="csl-left-margin">Brooke AM, Lambert PC, Burton PR, Clarke
C, Luyt DK, Simpson H. </span><span class="csl-right-inline">[Night
cough in a population-based sample of children: Characteristics,
relation to symptoms and associations with measures of asthma
severity.](https://www.ncbi.nlm.nih.gov/pubmed/8834336) *Eur Respir J*
1996;**9**:65–71.</span>

<span class="csl-left-margin">Halligan A, Lambert PC, O’Brien E, Shennan
A. </span><span class="csl-right-inline">[Characteristics of a reversed
circadian blood pressure rhythm in pregnant women with
hypertension.](https://www.ncbi.nlm.nih.gov/pubmed/8867569) *J Hum
Hypertens* 1996;**10**:135.</span>

<span class="csl-left-margin">Halligan A, Shennan A, Lambert PC, Swiet M
de, Taylor DJ. </span><span class="csl-right-inline">Diurnal blood
pressure difference in the assessment of preeclampsia. *Obstet Gynecol*
1996;**87**:205–8.
<https://doi.org/10.1016/0029-7844(95)00379-7>.</span>

<span class="csl-left-margin">Peek M, Shennan A, Halligan A, Lambert PC,
Taylor DJ, Swiet MD. </span><span class="csl-right-inline">Hypertension
in pregnancy: Which method of blood pressure measurement is most
predictive of outcome? *Obstet Gynecol* 1996;**88**:1030–3.
<https://doi.org/10.1016/S0029-7844(96)00350-X>.</span>

<span class="csl-left-margin">Brooke AM, Lambert PC, Burton PR, Clarke
C, Luyt DK, Simpson H. </span><span class="csl-right-inline">[The
natural history of respiratory symptoms in preschool
children.](https://www.ncbi.nlm.nih.gov/pubmed/8520749) *Am J Respir
Crit Care Med* 1995;**152**:1872–8.</span>

<span class="csl-left-margin">Esmail A, Lambert PC, Jones DR, Mitchell
EA. </span><span class="csl-right-inline">[Prevalence of risk factors
for sudden infant death syndrome in south east England before the 1991
national ’Back to Sleep’ health education
campaign.](https://www.ncbi.nlm.nih.gov/pubmed/8527180) *J Public Health
Med* 1995;**17**:282–9.</span>
