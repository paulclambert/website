Here are some references

------------------------------------------------------------------------

nocite: |<sup>[1](#ref-Syriopoulou2021a)</sup>
<sup>[2](#ref-Sweeting2021)</sup><sup>[3](#ref-Lambert2015)</sup>

<span class="csl-left-margin">Syriopoulou E, Rutherford MJ, Lambert PC.
</span><span class="csl-right-inline">Inverse probability weighting and
doubly robust standardization in the relative survival framework.
*Statistics in Medicine* 2021.
<https://doi.org/10.1002/sim.9171>.</span>

<span class="csl-left-margin">Sweeting MJ, Oliver-Williams C, Teece L,
Welch CA, Belder MA de, Coles B, Lambert PC, Paley L, Rutherford MJ,
Elliss-Brookes L, Deanfield J, Peake MD, Adlam D. </span><span
class="csl-right-inline">Data resource profile: The Virtual
Cardio-Oncology Research Initiative (VICORI) linking national English
cancer registration and cardiovascular audits. *International Journal of
Epidemiology* 2022;**50**:1768–79.
<https://doi.org/10.1093/ije/dyab082>.</span>

<span class="csl-left-margin">Lambert PC, Dickman PW, Rutherford MJ.
</span><span class="csl-right-inline">Comparison of approaches to
estimating age-standardized net survival. *BMC Med Res Methodol*
2015;**15**:64. <https://doi.org/10.1186/s12874-015-0057-3>.</span>
