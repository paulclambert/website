// Updates to standsurv

0.1 -  2019-1-20
       test version released on webpage 20/1/2019
0.2 -  2019-03-04
0.34 - 2019-07-03
0.36 - 2019-11-07

0.43 - 2019/02/29
       -- fixed bug with no at option - now behaves as at(.)
       -- added derivative of hazard functions for streg models
       -- now work with ode calculations for competing risks.
       -- error given if a cure models 
       -- fixed some typos in help file.

0.44 - now compiled in 15.1		   

0.45 - 2020-06-09
     - fixed problem with more than 2 competing risks models
     - added error of missing indweights

0.46 - 2020-06-11
     - genind works for competing risks models
     - fixed cif bug (from 0.45)
     
0.47 - 2020-11-28
     - toffset() option now works again
     - contrast(ratio) returned log of ratio without ci option. Now fixed.
     - trans(loglog) gave lci and uci the wrong way round. Now fixed.
	 
0.48 - 2020-12-08
     - cif with rmft now works for competing risks models to estimate loss in life expectancy.     
     
0.50 - 2021-02-03
     - Fixed bug introduced in last release that ode methods for crude probabilities and CIFS gave
       incorrect estimates if not sorted by timevar.
       
0.51 - crude probabilities use ode method by default
     - can now use file from web location in expsurv(using()) option.       
 
0.52 - help file edit + genind() now works when calculating cifs in competing risks models
        
0.53 - fixed bug when calculation ci or se for rmst for exponential models
   
0.55 - for crude probabilities of death - contrast was not returned without ci option - fixed.
       
0.56 - 2021-03-21
       -- popmort filename can now have spaces for windows compatability.
       -- genind option now works for most predictions.	   
	   -- at() suboption of expsurv now works with multiple variables.
	   
0.57 - 2021-03-22
       -- genind() option for competing risks and crude probabilities now allows if statements.

0.58 - 2021-04-09
       -- expsurv option when combined with ode did not use expected rates       
	   
0.59 - 2021-05-10
       -- introduced a bug in previous version for CIs when doing contrasts - now fixed.
       
0.60 - 2021-05-12
       -- too aggressive error checks for attimevar suboption removed.       
       
0.61 - 2021-08-23
       -- using -cif rmft- gave an unhelpful Mata error without the ci option - now fixed.
       -- ode evaluator now slightly more accurate. Works better with genind() option.
       
0.62 - 2021-09-03
       -- using -cif rmft- combined with a contrast gave an unhelpful Mata error without the ci option - now fixed.

0.63 - 2021-12-19
       -- stpm2 models using reverse option gave incorrect estimates - now fixed
       -- new suboption genind as part of expsurv option. 
       -- more meaningful errors for lincom() option if specified incorrectly 

0.64 - 2022-01-20
       -- reverse option now working with strcs models

0.65 - 2022-02-06
       -- previous update (0.63) broke genind option for standard models
	   
0.66 - 2022-04-06
       -- fixed error when combining with rmst, with expsurv and ode 
	   
0.67 - 2022-05-07
       -- add storeg option to save derivatives of standardized estimates.

0.69 - 2022-05-25
       -- add storeg option for competing risks / crude probabilities

0.71 - 2022-07-28
       -- now works with stpm3

0.77 - 2023-01-04
       -- compatable with @fn() and stpm3       
       
0.78 - 2023-01-22      
       -- added probit models for stpm3
       
0.80 - 2023-02-12
       -- fixed atif options from stpm3 models     
       -- Can now save predictions to frames
       
0.85 - 2023-03-08
       -- get now get expected survival for stpm3 lncumhazard models
       -- fixed error where frame would not be created if variable existed in current frame

0.86 - 2023-04-06
       -- added over() option      
	   
0.87 - 2023-04-14
       -- fixed bug in rmst of expectd survival in relative survival models
       
0.90 - 2023-04-28 
       -- added over() option       
       
0.92 - 2023-05-19
       -- minor bug with labels with over() option.     
	   
1.00 - 2023-05-21
       -- release for SSC	   
	  
1.01 - 2023-08-16	  
       -- can now list new variables before , as an alternative to atvars()
       
1.03 - 2023-09-01	  
       -- toffset now works with stpm3 models       
       
1.04 - 2023-09-19
       -- fixed issue with using different covariates in different competing
          risks models.
	     -- log hazard model predictions with untransformed time when using ode
	        now works
		  
1.05 - 2023-10-11
       -- stpm3 lnhazard competing models	can now predict allcause
          survival/failure
		  
1.06 - 2024-02-14
       -- problems with survival marginal expected survival fixed
		  