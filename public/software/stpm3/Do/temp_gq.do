frames reset
use https://www.pclambert.net/data/breast_nw if dep ==1, clear
stset survtime, failure(dead==1) exit(time 5)

set trace on
foreach nodes in 15 30 50 75 100 200 {
  stpm3 @ns(agediag,df(3)), scale(lnhazard) df(5) nodes(`nodes') ///
        /*tvc(@ns(agediag,df(3))) dftvc(2)*/ integoptions(allnum gl) 
  estimates store nodes`nodes'
  predict h`nodes'_50 h`nodes'_65 h`nodes'_90, hazard frame(hazard, mergecreate) timevar(0 5, n(101)) ///
                    at1(agediag 50) at2(agediag 65) at3(agediag(90)
  predict S`nodes'_50 S`nodes'_65 S`nodes'_90, surv frame(surv, mergecreate) timevar(0 5, n(101)) timevar(0 5, n(101)) ///
                    at1(agediag 50) at2(agediag 65) at3(agediag(90)
}

est tab nodes*, stats(ll)

frame hazard: scatter h* _t, msize(tiny..) yscale(log)

frame surv: scatter S* _t, msize(tiny..) yscale(log)