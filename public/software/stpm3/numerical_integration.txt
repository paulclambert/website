+++
date = "2023-04-23"
title = "Numerical Integration for log hazard models"
summary = "stpm3"
tags = ["stpm3","stpm2","survival","software","Stata"]
external_link = "" 
math = true
[header]
image = ""
caption = ""
+++

<<dd_do: quietly >>
capture mata mata drop fx()
frames reset
set scheme fpsaus_c
<</dd_do>>


## Models on the log hazard scale

In `stpm3` it is possible to fit models on the log hazard scale (`scale(lnhazard)`)
as well as the log cumulative hazard scale (`scale(lncumhazard)`).
Such models could not be fitted using `stpm2`.

Other Stata software that could be used fit models on the log hazard scale 
include `strcs`, `stgenreg` and `merlin`. One advantage of fitting
the models in `stpm3` is that the time take to fit the models is
much quicker, particularly when combined with the `python` option (LINK).

Models on the log hazard scale require numerical integration to be fitted.
The is because the transformation from log hazard to cumulative hazard
is not analytically tractable. The numerical integration used by `stpm3` 
is more accurate for many types of models than that used in other software (`strcs`, `merlin`)
through using a different type of numerical quadrature.

The log-likelihood for these models is as follows,

$$d_i \log\left[h(t_i|X_i)\right]- \int_{t_{0i}}^{t_i} h(u|X_i) du$$

As it is not possible to obtain the integral analytically we use numerical integration.

## Gaussian Quadrature

Gaussian quadrature is a method to perform numerical integration that converts 
an integral into a weighted sum over a set of predefined points known as nodes.

$$\int_{-1}^{-1} f(x) dx \approx \sum_{k=1}^K w_k f(x_k)$$

Thus we just need to evaluate the integrand, $f(x)$ at the $K$ nodes, $x_1 \ldots x_K$,
combine with the $K$ weights, $w_1 \ldots w_K$ and then sum. The larger the value of
$K$ the more accurate the approximation of the integral will be.

There are different ways to calculate the nodes and weights. The Wikipedia page
on Guassian Quadrature gives a good overview. [LINK]


The limits of the above integral are [-1,1], but can be transformed to any interval
using a change of variable rule. Thus to integrate $h(t|X_i)$ over $[t_{0i},t_i]$, 
we first generate nodes for a given $K$, for limits [-1,1], 
and than transform the nodes so we integrate over the limits  $[t_{0i},t_i]$ using

$$x_k' = \frac{t_i-t_{0i}}{2}x_k + \frac{t_i+t_{0i}}{2}$$


and then obtain the approximation to the integral using,


$$\int_{t_{0i}}^{t_i} h(u|X_i) du \approx \frac{t_i-t_{0i}}{2}\sum_{k=1}^K w_k h(x_k'|X_i) $$

Note that this integral need to be evaluated for all individuals in the model and the limits
change between individuals. When maximizing the log-likelihood function these integrals will
be need to be calculate multiple times until the model converges. For complex/awkward functions
many nodes may be required to obtain a desired level of accuracy.


## Gauss Legendre Quadrature

The most common way to perform the integration is Gauss Legendre Quadrature.
Below is an example of using gaussian quadrature to integrate $\int_0^2 \sqrt x$, 
using Gauss Legendre quadrature with 7 nodes. The integrals is evaluated at 4 
upper limits (0.5, 1, 1.5 and 2).

```stata
<<dd_do>>
mata:
// function to integrate
function fx(real matrix x) return(sqrt(x))
// generate Gauss Legendre nodes and weights
stpm3_quad_gl(7, weights=.,nodes=.)
weights, nodes
// integral limits
a = 0
b = range(0.5,2,0.5) 
// update nodes to limits
nodes2 = 0.5:*(b:-a):*J(rows(b),1,nodes') :+ 0.5:*(b:+a)
nodes2
// numeric integral
integral =  0.5:*(b:-a):*quadrowsum(fx(nodes2):*weights')
// analytical integral
true_integral = (b:^1.5):/1.5

// compare and relative difference.
b, integral, true_integral, reldif(true_integral,integral)
end
<</dd_do>>
```
To obtain greater accuracy more nodes would be required.

## Guass Legendre quadrature in `stpm3`

In `stpm3` it is possible to use Gauss Legendre quadrature in the estimation of then
cumulative hazard that is needed when maximizing the likelihood. This is what is
implemented in `stgenreg` and `merlin` as well as the R software `survPen`
and `mexhaz'. 

When implementing Guassian quadrature there needs to be a choice of the number of nodes, $K$
with more accurate estimates coming with high values of $K$, at the cost of being more
computationally intensive.

For well behaved hazard functions, Gauss Legendre quadrature generally works well,
but when the log hazard function is a spline function of log time then many nodes
may be required.

An example is shown below




## Three part integration in `stpm3`




## Tanhsinh quadrature





```stata
<<dd_do>>
use "https://pclambert.net/data/colon.dta", clear
stset surv_mm,f(status=1,2) id(id) scale(12) exit(time 120.5)
gen male = sex==1
<</dd_do>>
```


