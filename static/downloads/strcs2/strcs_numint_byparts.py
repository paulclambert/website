import jax.numpy as jnp   
import mladutil as mu

def python_ll(beta,X,wt,M):
  ## Parameters
  xb    = mu.linpred(beta,X,1)
  xbrcs = mu.linpred(beta,X,2)

  b0_first = jnp.matmul(M["lowrcs"],beta[1][:-1]) + beta[1][-1] + xb
  b1_first = jnp.matmul(M["lowdrcs"],beta[1][:-1])
  b0_first = b0_first - b1_first*M["lowttrans"]
  
  
  b0_last = jnp.matmul(M["highrcs"],beta[1][:-1]) + beta[1][-1] + xb
  b1_last = jnp.matmul(M["highdrcs"],beta[1][:-1])
  b0_last = b0_last - b1_last*M["highttrans"]
  
  ## first part analytic                                  
  cumhaz = M["includefirstint"]*jnp.where(M["hasbhtime"]
          ,(jnp.exp(b0_first)/b1_first*(jnp.exp(b1_first*M["lowt"])-jnp.exp(b1_first*M["t0"])))
          ,(jnp.exp(b0_first)/(b1_first+1)*(M["lowt"]**(b1_first+1)-M["t0"]**(b1_first+1))))
          
  ## secondpart - numerical
  ch_at_nodes = jnp.exp(jnp.matmul(M["allnodes"],beta[1][:-1]) + beta[1][-1] + xb)
  cumhaz = cumhaz + M["includesecondint"]*(0.5*(M["upperb"]-M["lowerb"]))*jnp.sum(M["weights"]*
                    ch_at_nodes,axis=1,keepdims=True)
  
  ## final part analytic
  cumhaz = cumhaz + M["includethirdint"]*jnp.where(M["hasbhtime"]
          ,(jnp.exp(b0_last)/b1_last*(jnp.exp(b1_last*M["t"])-jnp.exp(b1_last*M["hight"])))
          ,(jnp.exp(b0_last)/(b1_last+1)*(M["t"]**(b1_last+1)-M["hight"]**(b1_last+1))))
  
  # return log-likelhood
  return(jnp.sum(wt*(M["d"]*jnp.log(M["bh"] + jnp.exp(xb + xbrcs + jnp.log(M["t"]))) - cumhaz)))

