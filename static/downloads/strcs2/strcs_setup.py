from scipy.special import roots_legendre
import jax.numpy as jnp
from jax import vmap, jit
import mladutil as mu

def mlad_setup(M):
  nodes, weights = roots_legendre(M["Nnodes"])
  
  if(M["haspyallnumint"]): 
    nodes2 = 0.5*(M["t"] - M["t0"])*nodes + 0.5*(M["t"] + M["t0"])
  else: 
    nodes2 = 0.5*(M["upperb"] - M["lowerb"])*nodes + 0.5*(M["upperb"] + M["lowerb"])
  nodes2 = jnp.where(M["hasbhtime"],nodes2,jnp.log(nodes2))
  
  M["allnodes"] = mu.vrcsgen(nodes2,M["bhknots"][0],M["R_bh"])
  for i in range(1,int(M["Ntvc"])+1):
    M["allnodes"] = jnp.concatenate((M["allnodes"],M["tvc"+str(i)][:,:,None]*
                             mu.vrcsgen(nodes2 + M["tvcoff"+str(i)],M["tvcknots"+str(i)][0],
                             M["R_tvc"+str(i)])),axis=2)  
  M["weights"] =  weights 
  
  if(not M["haspyallnumint"]):
    M["lowttrans"]  = jnp.where(M["hasbhtime"],M["lowt"],jnp.log(M["lowt"]))
    M["highttrans"] = jnp.where(M["hasbhtime"],M["hight"],jnp.log(M["hight"]))

    M["lowrcs"]  = mu.vrcsgen(M["lowttrans"],M["bhknots"][0],M["R_bh"]) 
    M["lowdrcs"] = mu.vdrcsgen(M["lowttrans"],M["bhknots"][0],M["R_bh"])
    
    for i in range(1,int(M["Ntvc"])+1):
      M["lowrcs"] = jnp.concatenate((M["lowrcs"],M["tvc"+str(i)][:,:,None]*
                             mu.vrcsgen(M["lowttrans"] + M["tvcoff"+str(i)],M["tvcknots"+str(i)][0],
                             M["R_tvc"+str(i)])),axis=2)
      M["lowdrcs"] = jnp.concatenate((M["lowdrcs"]  ,M["tvc"+str(i)][:,:,None]*
                             mu.vdrcsgen(M["lowttrans"] + M["tvcoff"+str(i)],M["tvcknots"+str(i)][0],
                             M["R_tvc"+str(i)])),axis=2)    
                             
    M["highrcs"]  = mu.vrcsgen(M["highttrans"],M["bhknots"][0],M["R_bh"]) 
    M["highdrcs"] = mu.vdrcsgen(M["highttrans"],M["bhknots"][0],M["R_bh"])
    for i in range(1,int(M["Ntvc"])+1):
      M["highrcs"] = jnp.concatenate((M["highrcs"]  ,M["tvc"+str(i)][:,:,None]*
                             mu.vrcsgen(M["highttrans"] + M["tvcoff"+str(i)],M["tvcknots"+str(i)][0],
                             M["R_tvc"+str(i)])),axis=2)
      M["highdrcs"] = jnp.concatenate((M["highdrcs"]  ,M["tvc"+str(i)][:,:,None]*
                             mu.vdrcsgen(M["highttrans"] + M["tvcoff"+str(i)],M["tvcknots"+str(i)][0],
                             M["R_tvc"+str(i)])),axis=2)                             
                             
    
  return(M)

  
  
